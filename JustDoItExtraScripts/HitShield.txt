using UnityEngine;
using System.Collections;

public class HitShield : MonoBehaviour {

    void OnTriggerEnter(Collider _collider)
    {
        Debug.Log("Shield hit triggered");
        //minus shield health on any hit
        if(ShieldManager.shieldHealth >= 0)
            ShieldManager.LowerShieldHealth(10f);
        //goes less that zero just make it zero
        if (ShieldManager.shieldHealth < 0)
            ShieldManager.shieldHealth = 0;
        
        if (ShieldManager.shieldHealth <= 50f)
        {
            if (!ShieldManager.oneEnabled)
            {
                ShieldManager.oneEnabled = true;
                //ShieldManager.beginEnabled = true;
                ShieldMotor.firstRan = false;
            }
            ShieldManager.shieldsDestroy();
                
        }
        else if (ShieldManager.shieldHealth > 50f)
        {
            if (ShieldManager.oneEnabled)
            {
                ShieldManager.oneEnabled = false;
                ShieldManager.beginEnabled = false;
                ShieldMotor.firstRan = true;
            }
        }
    }
}
