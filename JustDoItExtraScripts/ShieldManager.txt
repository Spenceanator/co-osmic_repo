using UnityEngine;
using System.Collections;

public class ShieldManager : MonoBehaviour {

    static public GameObject frontShield;
    static public GameObject rearShield;
    static public GameObject rightShield;
    static public GameObject leftShield;

    static public float shieldHealth = 70f;

    static public bool oneEnabled;
    static public bool beginEnabled;

    public GameObject _frontShield;
    public GameObject _rearShield;
    public GameObject _leftShield;
    public GameObject _rightShield;

    void Start()
    {
        _frontShield = frontShield;
        //frontShield = _frontShield;
        //_rearShield = rearShield;
        //_rightShield = rightShield;
        //_leftShield = leftShield;
        oneEnabled = false;
        beginEnabled = false;
    }

    void Update()
    {
        _frontShield = frontShield;
        //frontShield = _frontShield;
        _rearShield = rearShield;
        _rightShield = rightShield;
        _leftShield = leftShield;
        Debug.Log(shieldHealth);
        Debug.Log(oneEnabled);
    }

    static public void shieldsDestroy()
    {
        if(shieldHealth <= 0)
        {
            frontShield.SetActive(false);
            rearShield.SetActive(false);
            rightShield.SetActive(false);
            leftShield.SetActive(false);
        }
    }
    
    static public void AddShieldHealth(float _shieldHealth)
    {
        shieldHealth += _shieldHealth;
    }
    static public void LowerShieldHealth(float _shieldHealth)
    {
        shieldHealth -= _shieldHealth;
    }
    static public void raiseShieldHealth(float _shieldHealth)
    {
        shieldHealth += _shieldHealth;
    }
    static public void ResetShieldHealth()
    {
        shieldHealth = 100;
    }

}
