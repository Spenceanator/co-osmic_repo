﻿using UnityEngine;
using System.Collections;

public class MediumAstroidController : MonoBehaviour {



    public GameObject smallAstroid;

    public GameObject expdEffect;


    [SerializeField]
    private float speed = 2f;
    [SerializeField]
    private float medWait = 1f;
    [SerializeField]
    private float waitColTime = 1.75f;

    [SerializeField]
    private float sWaitCol = 2f;


    [SerializeField]
    private bool collideSelf;

    [SerializeField]
    private int scoreToAdd = 25;

    public bool canDestroy = true;
	// Use this for initialization
	void Start () 
    {
        ////Physics.IgnoreLayerCollision(8, 8, true);
        //Physics.IgnoreLayerCollision(8, 9, true);
        //////Physics.IgnoreLayerCollision(9, 9, true);
        //Physics.IgnoreLayerCollision(8, 10, true);
        //////Physics.IgnoreLayerCollision(10, 10, true);  
        //Physics.IgnoreLayerCollision(9, 10, true);
        //var rotation = new Vector3(0, 0, Random.Range(-360, 360));
        //transform.Rotate(rotation);
        var newrotation = Quaternion.identity.z;
        newrotation += Random.Range(-45, 45);
        var rotation = new Vector3(0, 0, newrotation);

        transform.Rotate(rotation);

        //StartCoroutine(SpawnSaver(waitColTime));
        triggered = false;
        StartCoroutine(spawnCollide());

        speed += Random.Range(-1, 1);

        WaveManager.addAstroidSpawn(1);
    }

    //void OnEnable()
    //{
    //    var rotation = new Vector3(0, 0, Random.Range(-360, 360));
    //    transform.Rotate(rotation);
    //    StartCoroutine(SpawnSaver(waitColTime));
    //}
	
	// Update is called once per frame
	void Update ()
    {
       // transform.Rotate(0, 0, 0);
        
        transform.position += transform.up * speed * Time.deltaTime;

        //if (transform.position.z < 0f || transform.position.z > 0f)
        //{
        //    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        //    //GetComponent<Rigidbody>().MovePosition(new Vector3(GetComponent<Rigidbody>().position.x, GetComponent<Rigidbody>().position.y, 0));
        //}
    }

    IEnumerator spawnCollide()
    {
        GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(0.02f);
        GetComponent<Collider>().enabled = true;
    }
    //IEnumerator SpawnSaver(float _waitTime)
    //{
    //    Physics.IgnoreLayerCollision(8, 8, false);
    //    Physics.IgnoreLayerCollision(9, 9, false);
    //    Physics.IgnoreLayerCollision(10, 10, false);

    //    yield return new WaitForSeconds(_waitTime);
    //    Physics.IgnoreLayerCollision(8, 8, true);
    //    Physics.IgnoreLayerCollision(9, 9, true);
    //    Physics.IgnoreLayerCollision(10, 10, true);

    //}
    private bool triggered;
    /*
    void OnTriggerEnter(Collider _collider)
    {
        if (!triggered)
        {
            if (_collider.tag == "PlayerProjectile")
            {
                hit(_collider);
                ScoreManager.AddScore(scoreToAdd);
            }
            if(_collider.tag == "EnemyProjectile")
            {
                hit(_collider);
            }
            if(_collider.tag == "Shield" || _collider.tag == "mPlayer")
            {
                //StartCoroutine(ShieldcollideWait(sWaitCol));
                shieldHit(_collider);
            }
            if (_collider.tag == "Enemy")
            {
                hit(_collider);
                //WaveManager.addEnemyDestroy(1);
            }
           
        }
    }
   */
   void OnCollisionEnter(Collision _collider)
    {
        if (!triggered)
        {
            if(canDestroy)
            {
                if (_collider.collider.tag == "PlayerProjectile")
                {
                    hit(_collider.collider);
                    ScoreManager.AddScore(scoreToAdd);
                }
                if (_collider.collider.tag == "EnemyProjectile")
                {
                    hit(_collider.collider);
                }
                if (_collider.collider.tag == "Shield" || _collider.collider.tag == "mPlayer")
                {
                    //StartCoroutine(ShieldcollideWait(sWaitCol));
                    shieldHit(_collider.collider);
                }
                if (_collider.collider.tag == "Enemy")
                {
                    hit(_collider.collider);
                    //WaveManager.addEnemyDestroy(1);
                }
            }
        }
    }
    void OnTriggerEnter(Collider _collider)
    {
        if (!triggered)
        {
            if (canDestroy)
            {
                if (_collider.tag == "PlayerProjectile")
                {
                    hit(_collider);
                    ScoreManager.AddScore(scoreToAdd);
                }
                if (_collider.tag == "EnemyProjectile")
                {
                    hit(_collider);
                }
                if (_collider.tag == "Shield" || _collider.tag == "mPlayer")
                {
                    //StartCoroutine(ShieldcollideWait(sWaitCol));
                    shieldHit(_collider);
                }
                if (_collider.tag == "Enemy")
                {
                    hit(_collider);
                    //WaveManager.addEnemyDestroy(1);
                }
            }
        }
    }

    void shieldHit(Collider _collider)
    {
        triggered = true;
        //AstroidDestroy();
        Debug.Log("Hit");
        //Instantiate(expdEffect, transform.position, transform.rotation);
        Destroy(gameObject);
        //bulletDestroy();
        //Destroy(_collider.gameObject);

        WaveManager.addAstroidDestroy(1);
        Instantiate(smallAstroid, transform.position, transform.rotation);
        StartCoroutine(SmallInstatiate(medWait));
        Instantiate(smallAstroid, transform.position, transform.rotation);
        //WaveManager.addAstroidSpawn(2);
        //StartCoroutine(meduiumAWait(medWait));
        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
        //if(collideSelf == true)
        //{
        //    Physics.IgnoreLayerCollision(10, 10, false);
        //}
        //if (collideSelf == false)
        //{
        //    Physics.IgnoreLayerCollision(10, 10, true);
        //}
        //Physics.IgnoreLayerCollision(8, 8, true);
    }
    void hit(Collider _collider)
    {
        triggered = true;
        //AstroidDestroy();
        Debug.Log("Hit");
        //Instantiate(expdEffect, transform.position, transform.rotation);
        Destroy(gameObject);
        //bulletDestroy();
        //Destroy(_collider.gameObject);

        WaveManager.addAstroidDestroy(1);
        Instantiate(smallAstroid, transform.position, transform.rotation);
        StartCoroutine(SmallInstatiate(medWait));
        Instantiate(smallAstroid, transform.position, transform.rotation);
        //WaveManager.addAstroidSpawn(2);
        //StartCoroutine(meduiumAWait(medWait));
        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
        //Physics.IgnoreLayerCollision(8, 8, true);
    }

    
    IEnumerator SmallInstatiate(float _w)
    {
        //Transform AstroidDPos;
        //AstroidDPos = transform;
        //Physics.IgnoreLayerCollision(10, 10, false);
        //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
        collideSelf = true;
        yield return new WaitForSeconds(_w);
        //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
        //Physics.IgnoreLayerCollision(10, 10, true);
        collideSelf = false;
    }
   
}


