﻿using UnityEngine;
using System.Collections;

public class AstroidMeshRotate : MonoBehaviour {

    public float speed = 5f;

	// Use this for initialization
	void Start () {
        Vector3 randomDirection = new Vector3(Random.value, Random.value, Random.value);
        transform.Rotate(randomDirection);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
