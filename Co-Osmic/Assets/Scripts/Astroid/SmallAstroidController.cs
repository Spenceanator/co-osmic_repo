﻿using UnityEngine;
using System.Collections;

public class SmallAstroidController : MonoBehaviour {

    
    public GameObject expdEffect;

    public float speed = 2f;

    [SerializeField]
    private int scoreToAdd = 50;
    //public float medWait = 1f;
    //public float waitColTime = 1.75f;
    // Use this for initialization
    void Start()
    {
        WaveManager.addAstroidSpawn(1);
        ////Physics.IgnoreLayerCollision(8, 8, true);
        //Physics.IgnoreLayerCollision(8, 9, true);
        //////Physics.IgnoreLayerCollision(9, 9, true);
        //Physics.IgnoreLayerCollision(8, 10, true);
        //////Physics.IgnoreLayerCollision(10, 10, true);  
        //Physics.IgnoreLayerCollision(9, 10, true);
        //var rotation = new Vector3(0, 0, Random.Range(-360, 360));
        //transform.Rotate(rotation);
        //StartCoroutine(SpawnSaver(waitColTime));
        var newrotation = Quaternion.identity.z;
        newrotation += Random.Range(-45, 45);
        var rotation = new Vector3(0, 0, newrotation);

        transform.Rotate(rotation);

        triggered = false;
        Physics.IgnoreLayerCollision(10, 10, true);

        StartCoroutine(spawnCollide());

        speed += Random.Range(-1, 1);
    }

   
    // Update is called once per frame
    void Update()
    {
        

        transform.position += transform.up * speed * Time.deltaTime;

        
    }
    IEnumerator spawnCollide()
    {
        GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(0.02f);
        GetComponent<Collider>().enabled = true;
    }
    //IEnumerator SpawnSaver(float _waitTime)
    //{
    //    Physics.IgnoreLayerCollision(8, 8, false);
    //    Physics.IgnoreLayerCollision(9, 9, false);
    //    Physics.IgnoreLayerCollision(10, 10, false);

    //    yield return new WaitForSeconds(_waitTime);
    //    Physics.IgnoreLayerCollision(8, 8, true);
    //    Physics.IgnoreLayerCollision(9, 9, true);
    //    Physics.IgnoreLayerCollision(10, 10, true);

    //}
    private bool triggered;
    /*
    void OnTriggerEnter(Collider _collider)
    {
        if (!triggered)
        {
            if (_collider.tag == "PlayerProjectile")
            {
                hit(_collider);
                ScoreManager.AddScore(scoreToAdd);
            }
            if(_collider.tag == "EnemyProjectile")
            {
                hit(_collider);
            }
            if(_collider.tag == "Shield" || _collider.tag == "mPlayer")
            {
                triggered = true;
                Debug.Log("SmallHit");
                //Instantiate(expdEffect, transform.position, transform.rotation);
                Destroy(gameObject);

                WaveManager.addAstroidDestroy(1);
                Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
                WaveManager.CheckWaveOver();
            }
            if (_collider.tag == "Enemy")
            {
                hit(_collider);
                //WaveManager.addEnemyDestroy(1);
            }
            
        }
    } 
    */

    void OnCollisionEnter(Collision _collider)
    {
        if (!triggered)
        {
            if (_collider.collider.tag == "PlayerProjectile")
            {
                hit(_collider.collider);
                ScoreManager.AddScore(scoreToAdd);
            }
            if (_collider.collider.tag == "EnemyProjectile")
            {
                hit(_collider.collider);
            }
            if (_collider.collider.tag == "Shield" || _collider.collider.tag == "mPlayer")
            {
                triggered = true;
                Debug.Log("SmallHit");
                //Instantiate(expdEffect, transform.position, transform.rotation);
                Destroy(gameObject);

                WaveManager.addAstroidDestroy(1);
                Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
                WaveManager.CheckWaveOver();
            }
            if (_collider.collider.tag == "Enemy")
            {
                hit(_collider.collider);
                //WaveManager.addEnemyDestroy(1);
            }

        }
    }

    void hit(Collider _collider)
    {
        triggered = true;
        Debug.Log("SmallHit");
        //Instantiate(expdEffect, transform.position, transform.rotation);
        Destroy(gameObject);

        Destroy(_collider.gameObject);

        WaveManager.addAstroidDestroy(1);
        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
        WaveManager.CheckWaveOver();
    }
    
}
