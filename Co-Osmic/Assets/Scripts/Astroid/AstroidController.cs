﻿using UnityEngine;
using System.Collections;

public class AstroidController : MonoBehaviour {
    
    public GameObject mediumAstroid;
    public GameObject smallAstroid;
    public GameObject expdEffect;

    [SerializeField]
    private float speed = 2f;
    [SerializeField]
    private float medWait;
    [SerializeField]
    private float waitColTime = 1.75f;
    [SerializeField]
    private float sWaitCollide = 2f;

    [SerializeField]
    private bool collideSelf;

    [SerializeField]
    private int scoreToAdd = 10;
    //[SerializeField]
    //private int medScoreToAdd = 25;
    //[SerializeField]
    //private int smallScoreToAdd = 50;

    

    [SerializeField]
    private bool instMed;
    [SerializeField]
    private bool instSmall;

    
    private Camera cam;
    private float leftConstraint;
    private float rightConstraint;
    private float bottomConstraint;
    private float topConstraint;
    private float distanceZ = 30.0f;
    [SerializeField]
    private float spawnBuffer = 0.5f;

    [SerializeField]
    private GameObject MonObjectPooler;
    MonitorObjectPooler monitorPooler;

    private bool instCheck;
    private int instCounter;

    void Awake()
    {
        cam = Camera.main;
        distanceZ = Mathf.Abs(cam.transform.position.z + transform.position.z);
        leftConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).x;
        rightConstraint = -leftConstraint;
        bottomConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).y;
        topConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, distanceZ)).y;
        MonObjectPooler = GameObject.Find("MonitorObjectManager");
        monitorPooler = MonObjectPooler.GetComponent<MonitorObjectPooler>();

        instCheck = false;
        instCounter = 0;
    }

    void OnEnable()
    {   
        //StartCoroutine(SpawnSaver(waitColTime));
        triggered = false;
        //StartCoroutine(spawnCollide());

        speed += Random.Range(-1, 1);

       
        if(gameObject.tag == "LargeAstroid")
        {
            RandomSpawn();
        }
        else if(gameObject.tag != "LargeAstroid")
        {
            var rotation = new Vector3(0, 0, Random.Range(-360, 360));
            transform.Rotate(rotation);


        }
        if (instCounter >= 1)
        {
            instCheck = true;
            WaveManager.addAstroidSpawn(1);
        }
        instCounter++;
        
    }

   
	void Update ()
    {   
        transform.position += transform.up * speed * Time.deltaTime;   
    }

    void RandomSpawn()
    {
        int _Const = Random.Range(0, 3);
        float _topBotRandom = Random.Range(leftConstraint, rightConstraint);
        float _leftRightRandom = Random.Range(bottomConstraint, topConstraint);

        if (_Const == 0)
        {
            var rotation = new Vector3(0, 0, Random.Range(0, 180));
            transform.Rotate(rotation);
            transform.position = new Vector3(leftConstraint - spawnBuffer, _leftRightRandom, transform.position.z);
        }
        else if (_Const == 1)
        {
            var rotation = new Vector3(0, 0, Random.Range(180, 360));
            transform.Rotate(rotation);
            transform.position = new Vector3(rightConstraint + spawnBuffer, _leftRightRandom, transform.position.z);
        }
        else if (_Const == 2)
        {
            var rotation = new Vector3(0, 0, Random.Range(90, 270));
            transform.Rotate(rotation);
            transform.position = new Vector3(_topBotRandom, topConstraint + spawnBuffer, transform.position.z);
        }
        else if(_Const == 3)
        {
            var rotation = new Vector3(0, 0, Random.Range(-90, 90));
            transform.Rotate(rotation);
            transform.position = new Vector3(_topBotRandom, bottomConstraint - spawnBuffer, transform.position.z);
        }
    }
 
   
    private bool triggered;

    void OnCollisionEnter(Collision _collision)
    {
        Debug.Log(gameObject + "CollisionEnter");
        ManageHit(_collision.collider);
    }
    void OnTriggerEnter(Collider _collider)
    {
        Debug.Log(gameObject + "TriggerEnter");
        ManageHit(_collider);
    }

    void ManageHit(Collider _collider)
    {
        if (!triggered)
        {
            if (_collider.tag == "PlayerProjectile")
            {
                hit(_collider);
                ScoreManager.AddScore(scoreToAdd);
            }
            if (_collider.tag == "EnemyProjectile")
            {
                hit(_collider);
            }
            if (_collider.tag == "Shield" || _collider.tag == "mPlayer")
            {
                //StartCoroutine(ShieldCollideWait(sWaitCollide));
                hit(_collider);
            }
            if (_collider.tag == "Enemy")
            {
                hit(_collider);
            }
        }
        else
        {
            return;
        }
    }

    //IEnumerator ShieldCollideWait(float _secs)
    //{

    //    triggered = true;
    //    //AstroidDestroy();
    //    Debug.Log("Hit");
    //    //Instantiate(expdEffect, transform.position, transform.rotation);
    //    Destroy(gameObject);
    //    //bulletDestroy();

    //    WaveManager.addAstroidDestroy(1);
    //    //Instantiate(mediumAstroid, transform.position, transform.rotation);
    //    //StartCoroutine(MediumInstatiate(medWait));
    //    //Instantiate(mediumAstroid, transform.position, transform.rotation);
    //    //WaveManager.addAstroidSpawn(2);
    //    //StartCoroutine(meduiumAWait(medWait));
    //   
    //    //Physics.IgnoreLayerCollision(8, 8, true);
    //    //Physics.IgnoreLayerCollision(9, 15, true);
    //    yield return new WaitForSeconds(_secs);
    //    //Physics.IgnoreLayerCollision(9, 15, false);
    //}
    //void hitShield(Collider _collider)
    //{
    //    triggered = true;

    //    Debug.Log("Hit");

    //    //Destroy(gameObject);
    //    gameObject.SetActive(false);


    //    WaveManager.addAstroidDestroy(1);
    //    //Instantiate(mediumAstroid, transform.position, transform.rotation);

    //    //Instantiate(mediumAstroid, transform.position, transform.rotation);

    //}
    void hit(Collider _collider)
    {
        Debug.Log("Hit Triggered");
        
        Debug.Log("Hit");
        gameObject.SetActive(false);
        
        //if(!triggered)
        //{
        //    if(instMed)
        //    {
        //        SpawnMedium();
        //    }
        //    else if(instSmall)
        //    {
        //        SpawnSmall();
        //    }
        //}
        triggered = true;

    }
    //IEnumerator MediumInstatiate(float _w)
    //{
    //    //Transform AstroidDPos;
    //    //AstroidDPos = transform;

    //    //Physics.IgnoreLayerCollision(9, 9, false); set up to set if it can collide or not with each other
    //    collideSelf = true;
    //    //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
    //    yield return new WaitForSeconds(_w);
    //    //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
    //    collideSelf = false; //set to start by colliding then in _w time to stop colliding
    //    //Physics.IgnoreLayerCollision(9, 9, true);
    //}
   //IEnumerator spawnCollide()
   // {
   //     GetComponent<Collider>().enabled = false;
   //     yield return new WaitForSeconds(0.02f);
   //     GetComponent<Collider>().enabled = true;
   // }
    void SpawnMedium()
    {
        //Instantiate(mediumAstroid, transform.position, transform.rotation);
        //Instantiate(mediumAstroid, transform.position, transform.rotation);
        monitorPooler.SetActiveObjs(monitorPooler.MediumAsteroidObjects, monitorPooler.mediumAsteroid, 2, transform, null);
    }
    void SpawnSmall()
    {
        //Instantiate(smallAstroid, transform.position, transform.rotation);
        //Instantiate(smallAstroid, transform.position, transform.rotation);
        monitorPooler.SetActiveObjs(monitorPooler.SmallAsteroidObjects, monitorPooler.smallAsteroid, 2, transform, null);
    }

    void OnDisable()
    {
        triggered = false;
        if (instCheck)
        {
            WaveManager.addAstroidDestroy(1);
            if (instMed)
            {
                SpawnMedium();
            }
            else if (instSmall)
            {
                SpawnSmall();
            }
            Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
            WaveManager.CheckWaveOver();
        }
    }

}
