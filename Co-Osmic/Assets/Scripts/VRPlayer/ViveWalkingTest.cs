﻿using UnityEngine;
using System.Collections;

namespace VRTK
{
    public class ViveWalkingTest : MonoBehaviour
    {
        SteamVR_TrackedObject trackedObj;

        static public bool left;
        static public bool right;

        [SerializeField]
        private bool setRight;
        [SerializeField]
        private bool setLeft;

        //Should be a manager on the main one this is just the script that attaches to each controller
        //private bool setActive;

        // Use this for initialization
        void Awake()
        {
            trackedObj = GetComponent<SteamVR_TrackedObject>();
            left = false;
            right = false;

        }

        // Update is called once per frame
        void Update()
        {
            var device = SteamVR_Controller.Input((int)trackedObj.index);
            if (ViveWalkManager.setActive == false)
            {
                if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
                {
                    if (setRight == true)
                    {
                        right = true;
                    }
                    else if (setLeft == true)
                    {
                        left = true;
                    }

                }
            }
        }
    }
}
