﻿using UnityEngine;
using System.Collections;
using Valve.VR;

public class ViveTEsting : MonoBehaviour {
    SteamVR_TrackedObject trackedObj;


    public GameObject buttonHolder;
    public GameObject helpCanvas;
    public GameObject playerShipCollider;
    public GameObject debugObject;

    private bool buttonEnabled;
    private bool canvasEnabled;

    [SerializeField]
    private bool adminViveTest;
    private bool disabledShipCollider;

	// Use this for initialization
    void Awake()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
        buttonHolder.SetActive(false);
        helpCanvas.SetActive(true);
        buttonEnabled = false;
        canvasEnabled = true;
        debugObject.SetActive(false);

        
    }
	void Start () {
        
	}
	
	// Update is called once per frame
	void  Update () {
        var device = SteamVR_Controller.Input((int)trackedObj.index);
        if (device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            canvasEnabled = false;
            helpCanvas.SetActive(false);
            buttonHolder.SetActive(true);
        }
        else if(device.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu))
        {
            buttonHolder.SetActive(false);
        }
        if(device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
        {

            if (adminViveTest)
            {
                if (disabledShipCollider)
                {
                    playerShipCollider.SetActive(true);
                    disabledShipCollider = false;
                    debugObject.SetActive(false);
                }
                else if(!disabledShipCollider)
                {
                    playerShipCollider.SetActive(false);
                    disabledShipCollider = true;
                    debugObject.SetActive(true);
                }
            }
        }
	}
}
 /*
            if(device.GetPressDown(SteamVR_Controller.ButtonMask.ApplicationMenu))
            {
                canvasEnabled = false;
                helpCanvas.SetActive(false);
                if(buttonEnabled == false)
                {
                    buttonHolder.SetActive(true);
                    buttonEnabled = true;
                }
                else if(buttonEnabled == true)
                {
                    buttonHolder.SetActive(false);
                    buttonEnabled = false;
                }
            }
        */