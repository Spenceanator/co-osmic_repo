﻿using UnityEngine;
using System.Collections;
namespace VRTK
{
    public class ViveWalkManager : MonoBehaviour {

        static public bool setActive;

        void Awake()
        {
            setActive = false;
            //GetComponent<VRTK_TouchpadControl>().controller = false;
            //GetComponent<VRTK_TouchpadWalking>().leftController = false;
        }

        void Update()
        {
            if (!setActive)
            {
                
                if(ViveWalkingTest.left == true)
                {
                    //GetComponent<VRTK_TouchpadWalking>().deviceForDirection = VRTK_DeviceFinder.Devices.Left_Controller;
                    //GetComponent<VRTK_TouchpadWalking>().rightController = false;
                    //GetComponent<VRTK_TouchpadWalking>().leftController = true;
                    setActive = true;
                }
                else if(ViveWalkingTest.right == true)
                {
                    //GetComponent<VRTK_TouchpadWalking>().deviceForDirection = VRTK_DeviceFinder.Devices.Right_Controller;
                    //GetComponent<VRTK_TouchpadWalking>().leftController = false;
                    //GetComponent<VRTK_TouchpadWalking>().rightController = true;

                    setActive = true;
                }
            }
        }
    }
}
