﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ShipController : MonoBehaviour {

    //[SerializeField]
    //private float speed = 5f;
    [SerializeField]
    private ShipMotor pShip;

    public float accelerationForce = 10f;
    public float rotationForce = 3f;

    public Transform firePoint;
    public GameObject plrProjectile;
    public float fireDelay;

    public Collider shipCollider;

    public bool fullPower;
    public bool halfPower;
    public bool noPower;
    public bool canMove;

    //static public bool DoneBeenHit;

    //public GameObject frontShield;
    //public GameObject rightShield;
    //public GameObject leftShield;
    //public GameObject backShield;


    //public bool fireToggle = true;

    //public int pooledAmount = 20;
    //List<GameObject> bullets;

    
    private GameObject MonObjectPooler;
    MonitorObjectPooler monitorPooler;

    void Awake()
    {
        canMove = true;
        fullPower = true;
        halfPower = false;
        noPower = false;
        //bullets = new List<GameObject>();
        //for(int i =  0; i < pooledAmount; i++)
        //{
        //GameObject obj = (GameObject)Instantiate(plrProjectile);
        //obj.SetActive(false);
        //bullets.Add(obj);
        //}
        MonObjectPooler = GameObject.Find("MonitorObjectManager");
        monitorPooler = MonObjectPooler.GetComponent<MonitorObjectPooler>();
    }

    //fireWait
    //IEnumerator shotDelay()
    //{
    //    fireToggle = false;
    //    if(fireToggle == false)
    //    {
    //        Instantiate(plrProjectile, firePoint.position, firePoint.rotation);
    //        yield return new WaitForSeconds(fireDelay);
    //        fireToggle = true;
    //    }
    //}
   
	// Update is called once per frame
	void Update ()
    {    
        //astroids based movement
        if(LevelManager.gameBeOver)
        {
            canMove = false;
        }
        if (canMove)
        {
            float rotation = Input.GetAxisRaw("vHorizontal");
            float acceleration = Input.GetAxisRaw("Throttle");
            GetComponent<Rigidbody>().AddTorque(0, 0, rotation * rotationForce * accelerationForce * Time.deltaTime);
            

            if (fullPower == true)
            {
                GetComponent<Rigidbody>().AddForce(transform.up * acceleration * accelerationForce * Time.deltaTime);
            }
            else if (halfPower == true)
            {
                GetComponent<Rigidbody>().AddForce(transform.up * acceleration * accelerationForce * Time.deltaTime / 2);
            }
            else if (noPower)
            {
                GetComponent<Rigidbody>().AddForce(transform.up * 0);
                //do nothing there is no power to engines
            }
            Fire();
        }
        //if (DoneBeenHit == true)
        //{
        //    GetComponent<Collider>().enabled = false;
        //}
        //else if (DoneBeenHit == false)
        //{
        //    GetComponent<Collider>().enabled = true;
        //    //Debug.Log("Done been hit");
        //}
    }

    //void FixedUpdate()
    //{
    //    Fire();
    //}
    
    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.F))
        {
            //Instantiate(plrProjectile, firePoint.position, firePoint.rotation);        
            monitorPooler.SetActiveObjs(monitorPooler.PlayerProjectileObjects, monitorPooler.playerProjectile, 1, firePoint, firePoint);
        }

    }

    //MouseLookBasedMovement/Brackeystutorial()
        //{

            //var move = new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime, 0, 0);
            //float _xMov = Input.GetAxisRaw("Horizontal");
            //float _yMov = Input.GetAxisRaw("Vertical");

            //Vector3 _movHorizontal = transform.right * _xMov;
            //Vector3 _movVertical = transform.forward * _yMov;

            //Vector3 _velocity = (_movHorizontal + _movVertical).normalized * speed;

            //pShip.Move(_velocity);

            //var pos = Camera.main.WorldToScreenPoint(transform.position);
            //var dir = Input.mousePosition - pos;
            //var angle = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            //transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        //}

    //Dreadnoughtbasedmovement()
       //{


            ////var throttle = Input.GetAxis("Throttle");

            ////GetComponent<Rigidbody>().AddRelativeForce(move);
        //}
}
