﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HitShield : MonoBehaviour {

    void Update()
    {
       
    }

    public int wTime = 2;

    //public bool isHit;
    public bool canHit;

    public int damageToGive;

    //private GameObject player;
    private ShieldManager myShieldManager;
    private BlinkHit myBlinker;
    private ShieldMotor myShieldMotor;

    void Awake()
    {
        canHit = true;
        // player = transform.parent.gameObject;
        myShieldManager = transform.parent.GetComponent<ShieldManager>();
        myBlinker = transform.parent.GetComponent<BlinkHit>();
        myShieldMotor = transform.parent.GetComponent<ShieldMotor>();
    }
    
    void OnTriggerEnter(Collider _collider)
    {
        if (canHit)
        {
            canHit = false;
            Debug.Log("Shield hit triggered");
            //StartCoroutine(setCollision(wTime));
            //minus shield health on any hit
            if (myShieldManager.shieldHealth >= 0)
                myShieldManager.LowerShieldHealth(10f);
            //goes less that zero just make it zero
            if (myShieldManager.shieldHealth < 0)
                myShieldManager.shieldHealth = 0;
            myShieldManager.CheckShieldHealth();
            //isHit = true;
            myShieldMotor.shieldHit = true;

            myBlinker.shieldsBlinking = true;

            if(_collider.tag == "EnemyProjectile" || _collider.tag == "SmallEnemyProjectile")
            {
                _collider.gameObject.SetActive(false);
                //Destroy(_collider.gameObject);
            }
        }
        
        //if (myShieldManager.shieldHealth <= 50f)
        //{
        //    //if ()//!ShieldManager.oneEnabled)
        //    //{
        //    //    //ShieldManager.oneEnabled = true;
        //    //    //ShieldManager.beginEnabled = true;
        //    //    //ShieldMotor.firstRan = false;
        //    //}
        //    myShieldManagershieldsDestroy();
                
        //}
        //else if (myShieldManager.shieldHealth > 50f)
        //{
        //    //if ()//ShieldManager.oneEnabled)
        //    //{
        //    //    //ShieldManager.oneEnabled = false;
        //    //    //ShieldManager.beginEnabled = false;
        //    //    //ShieldMotor.firstRan = true;
        //    //}
        //}
    }
  
}
