﻿using UnityEngine;
using System.Collections;

public class BlinkHit : MonoBehaviour {

    public GameObject playerShipMesh;
    public GameObject gunObjMesh;
    public GameObject fMesh;
    public GameObject reMesh;
    public GameObject riMesh;
    public GameObject lMesh;

    private MeshRenderer shipMesh;
    private MeshRenderer gunMesh;
    private MeshRenderer frontMesh;
    private MeshRenderer rearMesh;
    private MeshRenderer rightMesh;
    private MeshRenderer leftMesh;

    public bool shieldsBlinking;
    public bool allBlinking;

    public float waitIntrvl = .2f;

    void Awake()
    {
        shipMesh = playerShipMesh.GetComponent<MeshRenderer>();
        if(gunObjMesh != null)
         gunMesh = gunObjMesh.GetComponent<MeshRenderer>();
        frontMesh = fMesh.GetComponent<MeshRenderer>();
        rearMesh = reMesh.GetComponent<MeshRenderer>();
        rightMesh = riMesh.GetComponent<MeshRenderer>();
        leftMesh = lMesh.GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if(allBlinking)
        {
            StartCoroutine(blinkingAll(waitIntrvl));
        }
        if(shieldsBlinking)
        {
            StartCoroutine(blinkingShields(waitIntrvl));
        }    
        
    }

    void MeshRenderAll(bool _tf)
    {
        shipMesh.enabled = _tf;
        if(gunObjMesh != null)
        {
            gunMesh.enabled = _tf;
        }
        frontMesh.enabled = _tf;
        rearMesh.enabled = _tf;
        rightMesh.enabled = _tf;
        leftMesh.enabled = _tf;
    }

    void MeshRenderShields(bool _tf)
    {
        frontMesh.enabled = _tf;
        rearMesh.enabled = _tf;
        rightMesh.enabled = _tf;
        leftMesh.enabled = _tf;
    }

    IEnumerator blinkingAll(float _p)
    {
        allBlinking = false;
        MeshRenderAll(false);
        yield return new WaitForSeconds(_p);
        MeshRenderAll(true);
        yield return new WaitForSeconds(_p);
        MeshRenderAll(false);
        yield return new WaitForSeconds(_p);
        MeshRenderAll(true);
        yield return new WaitForSeconds(_p);
        MeshRenderAll(false);
        yield return new WaitForSeconds(_p);
        MeshRenderAll(true);
        
        
    }
    IEnumerator blinkingShields(float _p)
    {
        shieldsBlinking = false;
        MeshRenderShields(false);
        yield return new WaitForSeconds(_p);
        MeshRenderShields(true);
        yield return new WaitForSeconds(_p);
        MeshRenderShields(false);
        yield return new WaitForSeconds(_p);
        MeshRenderShields(true);
        yield return new WaitForSeconds(_p);
        MeshRenderShields(false);
        yield return new WaitForSeconds(_p);
        MeshRenderShields(true);


    }
}
