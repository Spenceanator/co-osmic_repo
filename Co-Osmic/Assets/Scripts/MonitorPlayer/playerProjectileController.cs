﻿using UnityEngine;
using System.Collections;

public class playerProjectileController : MonoBehaviour {

    public float speed;

    //public GameObject expdEffect;
    //public GameObject mediumAstroid;
    //public GameObject smallAstroid;

   // public int pointsForKill;

    public int damageToGive;

    public float medWait;

    public float smallWait;
    
    public float destroyDelayCounter = 3f;

    public Vector3 lastPosition;
    public float DistanceTraveled;
    public float maxDistance = 20f;

    //float objectsVelocity;
    [SerializeField]
    private int predictionRange = 2;

    RaycastHit rayHit;

    void Awake()
    {
        //Physics.IgnoreLayerCollision(11, 15, true);
        


    }
    //void OnEnable()
    //{
    //    Physics.IgnoreLayerCollision(13, 15, true);
    //    lastPosition = transform.position;
    //}
	// Update is called once per frame
	void Update () {
        //GetComponent<Rigidbody>().AddForce(transform.forward * speed, ForceMode.VelocityChange);
        //GetComponent<Rigidbody>().velocity = new Vector3(transform.forward);
        transform.position += transform.forward * speed;
         

        DistanceTraveled += Vector3.Distance(transform.position, lastPosition);

        

        if (DistanceTraveled >= maxDistance)
        {
            gameObject.SetActive(false);
            //bulletDestroy();
        }
        if(triggered == true)
        {
            gameObject.SetActive(false);
        }

        /*
        float h = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float v = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        //        _transform.localPosition += _transform.right * h;
        //        _transform.localPosition += _transform.forward * v;

        Vector3 RIGHT = transform.TransformDirection(Vector3.right);
        Vector3 FORWARD = transform.TransformDirection(Vector3.forward);

        GetComponent<Rigidbody>().transform.localPosition += RIGHT * h;
        GetComponent<Rigidbody>().transform.localPosition += FORWARD * v;
        */

        //GetComponent<Rigidbody>().velocity = Vector3.up * speed;

        //GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().transform.localPosition.x * speed, 0, 0);

       
        //destroyDelayCounter -= Time.deltaTime;
        //if(destroyDelayCounter <= 0)
        //{
        //    Destroy(gameObject);
        //}


	}

    void OnEnable()
    {
        lastPosition = transform.position;
        triggered = false;
        collisionCheck = 0;
    }

    void FixedUpdate()
    {
        Vector3 rayOrigin = transform.position;
        Vector3 ray = GetComponent<Rigidbody>().transform.forward * speed * predictionRange;

        if (Physics.Raycast(rayOrigin, ray, out rayHit, ray.magnitude))
        {
            // The ray has hit the object will hit in "predictionRange" number of frames if its velocity remains constant.
            //waitDestroy();
            if (rayHit.collider.tag == "LargeAstroid" || rayHit.collider.tag == "MediumAstroid" || rayHit.collider.tag == "SmallAstroid")
            {
                if(rayHit.collider.gameObject.transform.parent != null)
                {
                    rayHit.collider.gameObject.transform.parent.gameObject.SetActive(false);
                }
                else
                {
                    rayHit.collider.gameObject.SetActive(false);
                }

                gameObject.SetActive(false);
            }
            else if(rayHit.collider.tag == "Enemy") //"LargeEnemy" || rayHit.collider.tag == "MedEnemy" || rayHit.collider.tag == "SmallEnemy")
            {
                //if(rayHit.collider.gameObject.transform.parent != null)
                //{
                //    rayHit.collider.gameObject.transform.parent.gameObject.GetComponent<EnemyHealthManager>().lowerHealth(damageToGive);
                //}
                if(rayHit.collider.gameObject.transform.parent == null)
                {
                    rayHit.collider.gameObject.GetComponent<EnemyHealthManager>().lowerHealth(damageToGive);
                }
                if(rayHit.collider.isTrigger)
                {
                    rayHit.collider.GetComponent<EnemyShipHit>().TriggerHit();
                    rayHit.collider.GetComponentInParent<EnemyHealthManager>().lowerHealth(damageToGive);
                    rayHit.collider.GetComponent<EnemyShipHit>().PlayerHit(damageToGive);
                }
                gameObject.SetActive(false);
            }
            else if(rayHit.collider.tag == "EnemyShield")
            {
                //if(rayHit.collider.gameObject.transform.parent != null)
                //{
                //    rayHit.collider.gameObject.transform.parent.gameObject.GetComponent<EnemyShieldHit>().damageTaken = damageToGive;
                //}
                if(rayHit.collider.gameObject.transform.parent == null)
                {
                    rayHit.collider.gameObject.GetComponent<EnemyShieldHit>().damageTaken = damageToGive;
                }
                if(rayHit.collider.isTrigger)
                {
                    rayHit.collider.GetComponent<EnemyShieldHit>().damageTaken = damageToGive;
                    rayHit.collider.GetComponent<EnemyShieldHit>().TriggerHit();
                }
                gameObject.SetActive(false);
            }
            //Destroy(gameObject);
        }
        //Debug.DrawRay(transform.position, ray, Color.green);

    }
    //IEnumerator waitDestroy()
    //{
    //    yield return new WaitForEndOfFrame();
    //    Destroy(gameObject);
    //}

    private bool triggered;
    //void ontriggerenter(collider _collide)
    //{
    //    if (!triggered)
    //    {
    //        if (_collide.tag != "mplayer")
    //        {
    //            triggered = true;
    //            destroy(gameobject);
    //        }
    //    }

    //}
    private int collisionCheck;

    void OnCollisionEnter(Collision _collide)
    {
        if(!triggered)
        {
            
            Debug.Log("Collision Check " + collisionCheck);
            if(_collide.collider.tag != "mPlayer")
            {
                collisionCheck += 1;
                triggered = true;
                //if(collisionCheck <= 1)
                //{
                //    Destroy(gameObject);
                //    if(_collide.collider.tag == "LargeAstroid" || _collide.collider.tag == "MediumAstroid" || _collide.collider.tag == "SmallAstroid") //Check to make sure that astroid can be destroyed
                //    {
                //        _collide.collider.gameObject.GetComponent<AstroidController>().CanDestroy = true; //Tell Astroid that it can be destroyed
                //    }

                //}
                //else if(collisionCheck > 1)
                //{
                //    if(_collide.collider.tag == "LargeAstroid")
                //    {
                //        _collide.collider.gameObject.GetComponent<AstroidController>().CanDestroy = false;
                //    }

                //    return;
                //}
                gameObject.SetActive(false);
            }
            
        }
    }

    void OnDisable()
    {
        DistanceTraveled = 0;
    }

    //IEnumerator meduiumAWait(float _wait)
    //{
    //    Instantiate(mediumAstroid, transform.position, transform.rotation);
    //    yield return new WaitForSeconds(_wait);
    //    Instantiate(mediumAstroid, transform.position, transform.rotation);
    //}

    //    IEnumerator MediumInstatiate(float _w)
    //{
    //    //Transform AstroidDPos;
    //    //AstroidDPos = transform;
    //    Physics.IgnoreLayerCollision(9, 9, false);
    //    //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
    //    yield return new WaitForSeconds(_w);
    //    //Instantiate(mediumAstroid, AstroidDPos.position, Quaternion.identity);
    //    Physics.IgnoreLayerCollision(9, 9, true);
    //}
    //void bulletDestroy()
    //    {
    //        gameObject.SetActive(false);
    //    }
    //void OnDisable()
    //{

    //}
   /* void OnTriggerExit(Collider other)
    {
        //other.GetComponent<
        //Physics.IgnoreCollision()
        if(other.tag == "LargeAstroid")
        {
            Debug.Log("Hit");  
            //Instantiate(expdEffect, transform.position, transform.rotation);
            Destroy(gameObject);
            //bulletDestroy();
            Destroy(other.gameObject);
            
            WaveManager.addAstroidDestroy(1);
            Instantiate(mediumAstroid, transform.position, Quaternion.identity);
            StartCoroutine(MediumInstatiate(medWait));
            Instantiate(mediumAstroid, transform.position, Quaternion.identity);
            WaveManager.addAstroidSpawn(2);
            //StartCoroutine(meduiumAWait(medWait));
            Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
        }
        else if(other.tag == "MediumAstroid")
        {
            Debug.Log("MedHit");
            //Instantiate(expdEffect, transform.position, transform.rotation);
            //Destroy(gameObject);
            bulletDestroy();
            WaveManager.addAstroidDestroy(1);
            Instantiate(smallAstroid, transform.position, Quaternion.identity);
            Destroy(other.gameObject);
            
            Instantiate(smallAstroid, transform.position, Quaternion.identity);
            WaveManager.addAstroidSpawn(2);
            Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
        }
        else if (other.tag == "SmallAstroid")
        {
            Debug.Log("SmallHit");
            //Instantiate(expdEffect, transform.position, transform.rotation);
            //Destroy(gameObject);
            bulletDestroy();
            Destroy(other.gameObject);
            
            WaveManager.addAstroidDestroy(1);
            Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
            WaveManager.CheckWaveOver();
        }
       
    }
     */

}
