﻿using UnityEngine;
using System.Collections;

public class ShieldManager : MonoBehaviour {


    [SerializeField]
    private bool adminOn;

    public float shieldHealth = 70f;

    private ShieldMotor myShieldMotor;

    void Awake()
    {

        shieldHealth = 70f;
        myShieldMotor = GetComponent<ShieldMotor>();
        
    }

    void Update()
    {
        if(adminOn == true)
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                AddShieldHealth(10f);
                CheckShieldHealth();
                Debug.Log(shieldHealth);
            }
            if (Input.GetKeyDown(KeyCode.V))
            {
                ResetShieldHealth();
                CheckShieldHealth();
                Debug.Log(shieldHealth);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Debug.Log(shieldHealth);
            }
            if (Input.GetKeyDown(KeyCode.N))
            {
                if (shieldHealth > 0)
                {
                    LowerShieldHealth(10f);
                }
                CheckShieldHealth();
                Debug.Log(shieldHealth);
            }
        }
        
    }

    
    
    public void AddShieldHealth(float _shieldHealth)
    {
        shieldHealth += _shieldHealth;
    }
    public void LowerShieldHealth(float _shieldHealth)
    {
        shieldHealth -= _shieldHealth;
    }
    public void raiseShieldHealth(float _shieldHealth)
    {
        shieldHealth += _shieldHealth;
    }
    public void ResetShieldHealth()
    {
        shieldHealth = 100;
    }


    public void CheckShieldHealth()
    {
        
        if(shieldHealth <= 50 && shieldHealth != 0) //Set start with one shield then change
        {
            if (myShieldMotor.firstShieldReset)
            {
                myShieldMotor.firstShieldEnabled = true;
            }
            myShieldMotor.shieldEnabled = true;
            myShieldMotor.allShieldsEnabled = true;
            
        }
        else if(shieldHealth <= 0)
        {
            myShieldMotor.allShieldsEnabled = false;
            myShieldMotor.firstShieldEnabled = true;
            myShieldMotor.firstShieldReset = true;//set reseter to false or true
        }
        else if (shieldHealth > 50)
        {
            myShieldMotor.shieldEnabled = false;
            myShieldMotor.firstShieldEnabled = false;
            myShieldMotor.allShieldsEnabled = true;
            
        }
       
    }
}
