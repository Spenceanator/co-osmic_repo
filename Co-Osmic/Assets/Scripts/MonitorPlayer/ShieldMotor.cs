﻿using UnityEngine;
using System.Collections;

public class ShieldMotor : MonoBehaviour {

    public bool shieldEnabled; //Just having one shield at a time
    public bool allShieldsEnabled; //absolute zero shields enabled
    public bool firstShieldEnabled; //set all shields to false except front ** Setter **
    public bool firstShieldReset; //reseter for all shields excpet front

    public GameObject frontShield;
    public GameObject rightShield;
    public GameObject leftShield;
    public GameObject rearShield;

    public bool frontActive;
    public bool leftActive;
    public bool rightActive;
    public bool rearActive;   

    public float wColTime = 2f;

    public bool shieldHit;

    public bool shipIsHit; //only for hitplayer script (fix later)

    private ShieldManager myShieldManager;
    private HitPlayer myHitPlayer;

    void Awake()
    {
        firstShieldEnabled = false;
        shieldEnabled = false;
        allShieldsEnabled = true;

        myShieldManager = GetComponent<ShieldManager>();
        myHitPlayer = GetComponentInChildren<HitPlayer>();
    }

	void Update () 
    {
       
        if(allShieldsEnabled)
        {
            if(firstShieldEnabled)
            {
                SetStartRemainShield();
                shieldEnabled = true;
            }
            if(shieldEnabled)
            {
                ShieldSetter();
            }
            else if(!shieldEnabled)
            {
                if(myShieldManager.shieldHealth >= 50)
                {
                    SetActivity(true, true, true, true);
                    
                    firstShieldReset = true;
                }
            }
        }
        else if(!allShieldsEnabled)
        {
            SetActivity(false, false, false, false);
        }
        if(shieldHit)
        {
            StartCoroutine(setCollision(wColTime));
        }
        
	}

    IEnumerator setCollision(float _w) 
    {
        //ShipController.DoneBeenHit = true;
        myHitPlayer.shipIsHit = true;
        shieldHit = false; //HitShield.isHit = false;
        SetShieldCol(false);
        Debug.Log("Shield colliders off");
        yield return new WaitForSeconds(_w);
        Debug.Log("Shield colliders on");
        SetShieldCol(true);
        //HitShield.isHit = false;
        //ShipController.DoneBeenHit = false;
        myHitPlayer.shipIsHit = false;
        SetShieldCanHit(true); //HitShield.canHit = true;
        
    }

    void SetShieldCanHit(bool _sb)
    {
        frontShield.GetComponent<HitShield>().canHit = _sb;
        rearShield.GetComponent<HitShield>().canHit = _sb;
        leftShield.GetComponent<HitShield>().canHit = _sb;
        rightShield.GetComponent<HitShield>().canHit = _sb;
    }

    void SetActivity(bool _shield1, bool _shield2, bool _shield3, bool _shield4) //Set which shields become active
    {
        frontShield.SetActive(_shield1);
        rightShield.SetActive(_shield2);
        leftShield.SetActive(_shield3);
        rearShield.SetActive(_shield4);
    }

    void SetShieldCol(bool _c) //set c ollider for shields for when they or player gets hit
    {
        frontShield.GetComponent<Collider>().enabled = _c;
        rightShield.GetComponent<Collider>().enabled = _c;
        leftShield.GetComponent<Collider>().enabled = _c;
        rearShield.GetComponent<Collider>().enabled = _c;
    }

    void SetStartRemainShield() //set all sheilds to false except front when starting
    {
        
        frontShield.SetActive(true);
        rightShield.SetActive(false);
        leftShield.SetActive(false);
        rearShield.SetActive(false);
        Debug.Log("First Shield Enabled");
        firstShieldEnabled = false;
        firstShieldReset = false;
        frontActive = true;
        //set frontshield true all else false
       
    }
    

    void ShieldSetter()
    { 
        if(frontActive == true)
        {
            SetActivity(true, false, false, false);
            //rightActive = false;
            //leftActive = false;
            //rearActive = false;
        }
        else if(rightActive == true)
        {
            SetActivity(false, true, false, false);
            //frontActive = false;
            //leftActive = false;
            //rearActive = false;
        }
        else if(leftActive == true)
        {
            SetActivity(false, false, true, false);
            //frontActive = false;
            //rightActive = false;
            //rearActive = false;
        }
        else if(rearActive == true)
        {
            SetActivity(false, false, false, true);
            //frontActive = false;
            //rightActive = false;
            //leftActive = false;
        }
       
        
    //{
    //    if(Input.GetButtonDown("FrontShield"))
    //    {
    //        SetActivity(true, false, false, false);
    //    }
    //    else if(Input.GetButtonDown("RightShield"))
    //    {
    //        SetActivity(false, true, false, false);
    //    }
    //    else if(Input.GetButtonDown("LeftShield"))
    //    {
    //        SetActivity(false, false, true, false);
    //    }
    //    else if(Input.GetButtonDown("RearShield"))
    //    {
    //        SetActivity(false, false, false, true);
    //    }
    }
   

    
    
}
