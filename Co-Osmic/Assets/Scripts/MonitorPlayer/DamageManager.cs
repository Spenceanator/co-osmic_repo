﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageManager : MonoBehaviour {

    public GameObject PlayerProjectile;

    private playerProjectileController projectileController;

    private HitShield frontShield;
    private HitShield rearShield;
    private HitShield leftShield;
    private HitShield rightShield;

    private HitPlayer playerHit;

    private GameObject objFrontShield;
    private GameObject objRearShield;
    private GameObject objLeftShield;
    private GameObject objRightShield;
    private GameObject objPlayerCollider;

    [SerializeField]
    private int EasyProjectileDamage;
    [SerializeField]
    private int EasyShieldDamage;
    [SerializeField]
    private int EasyPlayerDamage;

    [SerializeField]
    private int MediumProjectileDamage;
    [SerializeField]
    private int MediumShieldDamage;
    [SerializeField]
    private int MediumPlayerDamage;

    [SerializeField]
    private int HardProjectileDamage;
    [SerializeField]
    private int HardShieldDamage;
    [SerializeField]
    private int HardPlayerDamage;

    void Awake()
    {
        objFrontShield = transform.FindChild("FrontShield").gameObject;
        objRearShield = transform.FindChild("RearShield").gameObject;
        objLeftShield = transform.FindChild("LeftShield").gameObject;
        objRightShield = transform.FindChild("RightShield").gameObject;

        objPlayerCollider = transform.Find("ShipCollider").gameObject;

        frontShield = objFrontShield.GetComponent<HitShield>();
        rearShield = objRearShield.GetComponent<HitShield>();
        leftShield = objLeftShield.GetComponent<HitShield>();
        rightShield = objRightShield.GetComponent<HitShield>();

        playerHit = objPlayerCollider.GetComponent<HitPlayer>();

        projectileController = PlayerProjectile.GetComponent<playerProjectileController>();
    }

    public void SetEasy()
    {
        projectileController.damageToGive = EasyProjectileDamage;

        frontShield.damageToGive = EasyShieldDamage;
        rearShield.damageToGive = EasyShieldDamage;
        leftShield.damageToGive = EasyShieldDamage;
        rightShield.damageToGive = EasyShieldDamage;

        playerHit.damageToGive = EasyPlayerDamage;
    }

    public void SetMedium()
    {
        projectileController.damageToGive = MediumPlayerDamage;

        frontShield.damageToGive = MediumShieldDamage;
        rearShield.damageToGive = MediumShieldDamage;
        leftShield.damageToGive = MediumShieldDamage;
        rightShield.damageToGive = MediumShieldDamage;

        playerHit.damageToGive = MediumPlayerDamage;
    }

    public void SetHard()
    {
        projectileController.damageToGive = HardPlayerDamage;

        frontShield.damageToGive = HardShieldDamage;
        rearShield.damageToGive = HardShieldDamage;
        leftShield.damageToGive = HardShieldDamage;
        rightShield.damageToGive = HardShieldDamage;

        playerHit.damageToGive = HardPlayerDamage;
    }




}
