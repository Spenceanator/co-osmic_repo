﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HitPlayer : MonoBehaviour {

	public int damageToGive;

	public bool leftEngineHit; //when set to true say it is hit and damaged, then set hit to false
	public bool rightEngineHit;


	//public GameObject leftECol; //only used for setting collision to off
	//public GameObject rightECol;
	
	public bool canHit;
	public bool shipIsHit;

	public bool allDamaged;
	//public bool liDamaged;
	public bool tookDamage;
	public List<int> roomNumbers = new List<int>();

	bool lifeAdded;
	bool reactAdded;
	bool leftAdded;
	bool rightAdded;
	bool shieldAdded;

	private BlinkHit blinker;
	//private HitShield myHitShield;
	private ShieldMotor myShieldMotor;

	// Use this for initialization
	void Awake () 
	{
		canHit = true;
		shipIsHit = false;
		allDamaged = false;
		
		//liDamaged = false;
		
		roomNumbers.Add(0);
		roomNumbers.Add(1);
		roomNumbers.Add(2);
		roomNumbers.Add(3);
		roomNumbers.Add(4);

		lifeAdded = true;
		reactAdded = true;
		leftAdded = true;
		rightAdded = true;
		shieldAdded = true;

		blinker = gameObject.GetComponentInParent<BlinkHit>();
		myShieldMotor = gameObject.GetComponentInParent<ShieldMotor>();


	}
	
	// Update is called once per frame
	void Update ()
	{

		if(shipIsHit == true)
		{
			canHit = false;
			GetComponent<Collider>().enabled = false;
			//if(leftEngineHit == false)
			//{
			//    leftECol.GetComponent<Collider>().enabled = false;
			//}
			//if(rightEngineHit == false)
			//{
			//    rightECol.GetComponent<Collider>().enabled = false;
			//}
			
		}
		else if (shipIsHit == false)
		{
			canHit = true;
			GetComponent<Collider>().enabled = true;
			//if (leftEngineHit == false)
			//{
			//    leftECol.GetComponent<Collider>().enabled = true;
			//}
			//if (rightEngineHit == false)
			//{
			//    rightECol.GetComponent<Collider>().enabled = true;
			//}   
		}
		if (LifeSupportRoom.RoomDamaged == true && LeftEngineRoom.RoomDamaged == true && RightEngineRoom.RoomDamaged == true && ReactorRoom.RoomDamaged == true && ShieldRoom.RoomDamaged == true)
		{   //procede to blow shit up
			//gameObject.SetActive(false);
			//run end game
			LevelManager.gameBeOver = true;
			allDamaged = true;
			//Debug.Log("Game be over");
		}



		if (LifeSupportRoom.RoomDamaged == false && lifeAdded == false)
		{
			roomNumbers.Add(0);
			lifeAdded = true;
		}
		if (ReactorRoom.RoomDamaged == false && reactAdded == false)
		{
			roomNumbers.Add(1);
			reactAdded = true;
		}
		if(LeftEngineRoom.RoomDamaged == false && leftAdded == false)
		{
			roomNumbers.Add(2);
			leftAdded = true;
		}
		if(RightEngineRoom.RoomDamaged == false && rightAdded == false)
		{
			roomNumbers.Add(3);
			rightAdded = true;
		}
		if(ShieldRoom.RoomDamaged == false && shieldAdded == false)
		{
			roomNumbers.Add(4);
			shieldAdded = true;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if(canHit)
		{
			blinker.allBlinking = true;
			myShieldMotor.shieldHit = true;
		}
	}

	public void ApplyDamage(int _damage)
	{

	}
	/*
	void OnTriggerEnter(Collider other)
	{
 
		if(canHit)
		{
			tookDamage = false;
			canHit = false;
			blinker.allBlinking = true;
			//Debug.Log("Ship Hit triggered");
			shipIsHit = true;
			int roomChoose = roomNumbers[Random.Range(0, roomNumbers.Count)];
			//Debug.Log(roomNumbers);
			myShieldMotor.shieldHit = true; //HitShield.isHit = true;
			if (other.tag == "EnemyProjectile" || other.tag == "SmallEnemyProjectile")
			{   
				Destroy(other.gameObject);
				
			}
			
			
		   //while(allDamaged == false)
		   //{
		   //    roomChoose = Random.Range(0, 5);
		   // while(tookDamage == false)
		   // {
			if (allDamaged == false)
			{
				if (roomChoose == 0)
				{
					roomNumbers.Remove(0);
					lifeAdded = false;
					if (LifeSupportRoom.RoomDamaged == false)
					{
						LifeSupportRoom.RoomDamaged = true;
						if (LifeSupportRoom.roomFixed == true) //double checking to make sure it damges the room
						{
							LifeSupportRoom.roomFixed = false;
							LifeSupportRoom.RoomDamaged = true;
							//roomNumbers.Add(0);
							//tookDamage = true;
							//break;
						}
					}
					else if (LifeSupportRoom.RoomDamaged == true)
					{
						//roomChoose = Random.Range(0, 5);
						//break;

					}
				}
				else if (roomChoose == 1)
				{
					roomNumbers.Remove(1);
					reactAdded = false;
					if (ReactorRoom.RoomDamaged == false)
					{
						ReactorRoom.RoomDamaged = true;
						if (ReactorRoom.roomFixed == true) //double checking to make sure it damges the room
						{
							ReactorRoom.roomFixed = false;
							ReactorRoom.RoomDamaged = true;
							//roomNumbers.Add(1);
							//tookDamage = true;
							//break;
							
						}
					}
					else if (ReactorRoom.RoomDamaged == true)
					{
						//roomChoose = Random.Range(0, 5);
						//break;
					}
				}
				else if (roomChoose == 2)
				{
					roomNumbers.Remove(2);
					leftAdded = false;
					if (LeftEngineRoom.RoomDamaged == false)
					{
						LeftEngineRoom.RoomDamaged = true;
						if (LeftEngineRoom.roomFixed == true)
						{
							LeftEngineRoom.roomFixed = false;
							LeftEngineRoom.RoomDamaged = true;
							//roomNumbers.Add(2);
							//tookDamage = true;
							//break;
						}
					}
					else if (LeftEngineRoom.RoomDamaged == true)
					{
						//roomChoose = Random.Range(0, 5);
						//break;
					}
				}
				else if (roomChoose == 3)
				{
					roomNumbers.Remove(3);
					rightAdded = false;
					if (RightEngineRoom.RoomDamaged == false)
					{
						RightEngineRoom.RoomDamaged = true;
						if (RightEngineRoom.roomFixed == true)
						{
							RightEngineRoom.roomFixed = false;
							RightEngineRoom.RoomDamaged = true;
							//roomNumbers.Add(3);
							//  tookDamage = true;
							//break;
						}
					}
					else if (RightEngineRoom.RoomDamaged == true)
					{
						//roomChoose = Random.Range(0, 5);
						//break;
					}
				}
				else if (roomChoose == 4)
				{
					roomNumbers.Remove(4);
					shieldAdded = false;
					//Debug.Log("Shield Room down");
					if (ShieldRoom.RoomDamaged == false)
					{
						ShieldRoom.RoomDamaged = true;
						if (ShieldRoom.roomFixed == true)
						{
							ShieldRoom.roomFixed = false;
							ShieldRoom.RoomDamaged = true;
							//roomNumbers.Add(4);
							//  tookDamage = true;
							//break;
						}
					}
					else if (ShieldRoom.RoomDamaged == true)
					{
						//Debug.Log("Shield room room damaged already");
						//roomChoose = Random.Range(0, 5);
						//break;
					}
					else
					{
						//Debug.Log("Gay retard error");
					}
				}
				if (roomNumbers.Capacity == 0)
				{
					Debug.LogError("Room choose Capacity hit zero");
					//Debug.Log(roomChoose);
					//break;

				}
			}
				//Debug.Log(roomChoose);
			}
		   //    if(tookDamage == true)
		   //    {
		   //        //Debug.Log("Esacaped while loops");
		   //        //break;
				   
		   //    }
		   //}
		}
		*/

	//if(/*choose random room to damage*/)
	//{
	//    //roll random number
	//    //check to see
	//          //if same as damaged roll again
	//      //tell system it is damaged 
	//}



}
		
	
