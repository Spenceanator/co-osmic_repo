﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightMessanger))]
public class PhysicalValve : MonoBehaviour {

	#region Public Variables
	[Header("Axis:")]
	[SerializeField]
	private bool x;
	[SerializeField]
	private bool y;
	[SerializeField]
	private bool z;
	[Space(5)]
	[SerializeField]
	private float currentRotation;
	[SerializeField]
	private float goalRotation;
	[Space(5)]
	[SerializeField]
	private float threshHoldRange; //Half this number when thinking of a threshold range.
	[SerializeField]
	private float sfxRange; //Based on how far wheel has moved between updates
	#endregion

	#region Private Variables
	private RandomSound myRandSound;
	private bool newRange;
	private float rotLastFrame;
	private LightMessanger myLightMsngr;
	#endregion

	// Use this for initialization
	void Awake() {
		if (gameObject.GetComponent<RandomSound>() != null)
		{
			myRandSound = gameObject.GetComponent<RandomSound>();
		}
		myLightMsngr = gameObject.GetComponent<LightMessanger>();
		SetFixThreshold();
	}

	// Update is called once per frame
	void FixedUpdate() {
		#region Update Axis
		if (x)
		{
			currentRotation = transform.localEulerAngles.x;

		}
		else if (y)
		{
			currentRotation = transform.localEulerAngles.y;
		}
		else if (z)
		{
			currentRotation = transform.localEulerAngles.z;
		}
		else
		{
			Debug.LogError("Invalid axis selection on " + gameObject.name);
		}
		#endregion

		if (!myLightMsngr.CheckGreenActive() && !newRange) //Checks if needs to set new range and does
		{
			SetFixThreshold();
		}
		if (currentRotation != rotLastFrame)
		{
			CheckRange();
			CheckPlaySound();
			//Debug.LogWarning("Last rotation thing " + rotLastFrame);
		}
		rotLastFrame = currentRotation;

	}

	public void SetGreenActive(bool _active)
	{
		//myLightMsngr.greenActive = _active;
		myLightMsngr.SetGreenActive(_active);
		if(!_active)
		{
			SetFixThreshold();
		}
		else if(_active)
		{
			goalRotation = currentRotation;
			CheckRange();
		}
	}

	public void SetFixThreshold()
	{
		bool _canAdd = false;
		bool _canSub = false;

		if (currentRotation + threshHoldRange < 360f)
			_canAdd = true;
		if (currentRotation - threshHoldRange > -360f)
			_canSub = true;

		if (_canAdd || _canSub)
		{
			newRange = true;
			if (_canAdd && _canSub)
			{
				goalRotation = Random.Range(currentRotation - threshHoldRange, currentRotation + threshHoldRange);
			}
			else if (_canAdd && !_canSub)
			{

				for (;;)
				{
					goalRotation = Random.Range(currentRotation + threshHoldRange, 360f);
					if (goalRotation + threshHoldRange <= 360f)
					{
						break;
					}

				}
			}
			else if (!_canAdd && _canSub)
			{
				for (;;)
				{
					goalRotation = Random.Range(0f, currentRotation - threshHoldRange);
					if (goalRotation - threshHoldRange >= 0f)
					{
						break;
					}
				}

			}
		}
		else
		{
			Debug.LogError("Invalid angle to receive threshold on " + gameObject);
		}
	}

	//public void SetFixThreshold() //I did not think eular angles through kms
	//{
	//	bool _canAdd = false; //Bool for going up 90 degrees
	//	bool _canSub = false; //Float for going down 90 degrees


	//	if (currentRotation + threshHoldRange < 360f)
	//		_canAdd = true;
	//	if (currentRotation - threshHoldRange > -360f)
	//		_canSub = true;

	//	if (_canAdd || _canSub)
	//	{
	//		newRange = true;
	//		if (_canAdd && _canSub)
	//		{
	//			goalRotation = Random.Range(-360f, 360f);
	//		}
	//		else if (_canAdd && !_canSub)
	//		{

	//			for (;;)
	//			{
	//				goalRotation = Random.Range(currentRotation, 360f);
	//				if (goalRotation + threshHoldRange <= 360f)
	//				{
	//					break;
	//				}

	//			}
	//		}
	//		else if (!_canAdd && _canSub)
	//		{
	//			for (;;)
	//			{
	//				goalRotation = Random.Range(-360f, currentRotation);
	//				if (goalRotation - threshHoldRange >= -360f)
	//				{
	//					break;
	//				}
	//			}

	//		}
	//	}
	//	else
	//	{
	//		Debug.LogError("Invalid angle to receive threshold on " + gameObject);
	//	}

	//}

	private void CheckRange()
	{
		float _botRange = goalRotation - threshHoldRange;
		float _topRange = goalRotation + threshHoldRange;

		if (currentRotation >= _botRange && currentRotation <= _topRange)
		{
			myLightMsngr.SetGreenActive(true);
		}
		else
		{
			myLightMsngr.SetGreenActive(false);
		}
	}

	private void CheckPlaySound() //On movement check last rot with current then call RandomSound
	{
		float _Difference = (currentRotation * -1) + rotLastFrame;
		_Difference = Mathf.Abs(_Difference);

		if (_Difference >= sfxRange)
			myRandSound.PlayRandomSFX();
	}
	
}
