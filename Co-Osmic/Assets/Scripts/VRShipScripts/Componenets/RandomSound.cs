﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class RandomSound : MonoBehaviour {

	public List<AudioClip> clipList;

	[SerializeField]
	private AudioSource myAudSrc;

	void Awake()
	{
		//clipList = new List<AudioClip>();
		myAudSrc = gameObject.GetComponent<AudioSource>();
		if (myAudSrc.playOnAwake)
		{
			gameObject.AddComponent<AudioSource>();
			myAudSrc = gameObject.GetComponent<AudioSource>();
			myAudSrc.playOnAwake = false;
			myAudSrc.spatialize = true;
			Debug.LogWarning("Audio source componenet not properly configured on " + gameObject.name + "!");
		}
	}

	public void PlayRandomSFX()
	{
		int _rand = Random.Range(0, clipList.Count);
		if (myAudSrc.isPlaying)
			return;
		myAudSrc.PlayOneShot(clipList[_rand]);
	}


}
