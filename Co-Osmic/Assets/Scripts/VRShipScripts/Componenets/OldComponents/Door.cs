﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
  
    private Animator anim;

    public string ColliderTag;
    
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Open", false);
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == ColliderTag)
        {
            anim.SetBool("Open", true);
        }
        //if transitioning play sound
         
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == ColliderTag)
        {
            anim.SetBool("Open", false);
        }
    }
}
