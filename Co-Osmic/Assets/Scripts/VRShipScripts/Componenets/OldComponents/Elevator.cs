﻿using UnityEngine;
using System.Collections;

public class Elevator : MonoBehaviour {

    [SerializeField]
    private Animator anim;
	
	void Start () {
        Stop();
	}

    public void ToFloor1()
    {
        anim.SetBool("ToFloor1", true);
        StartCoroutine(TStop());
        
   
    }

    public void ToFloor2()
    {
        anim.SetBool("ToFloor2", true);
        StartCoroutine(TStop());
      
    }

    public void ToFloor3()
    {
        anim.SetBool("ToFloor3", true);
        StartCoroutine(TStop());
       
    }


    IEnumerator TStop()
    {
        yield return new WaitForSeconds(1f);
        Stop();
    }


    public void Stop()
    {
            anim.SetBool("ToFloor1", false);
            anim.SetBool("ToFloor2",  false);
            anim.SetBool("ToFloor3",  false);     
    }
}
