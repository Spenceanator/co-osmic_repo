﻿using UnityEngine;
using System.Collections;



//namespace VRStandardAssets.Examples
//{
  public class ButtonD : MonoBehaviour
  {


    static public bool Pressed;

    public bool LifeSupport;
        public bool lGreenButton;
        public bool lRedButton;
        public bool lBlueButton;

    public bool LeftEngine;
        public bool leGreenButton;
        public bool leRedButton;
        public bool leBlueButton;

    public bool RightEngine;
        public bool reGreenButton;
        public bool reRedButton;
        public bool reBlueButton;

    public bool ShieldRm;
        public bool sGreenButton;
        public bool sRedButton;
        public bool sBluebutton;

    public bool ReactorRm;
        public bool rrGreenButton;
        public bool rrRedButton;
        public bool rrBlueButton;
        

    public bool Teleporter;
        public bool teLifeRoom;
        public bool teLeRoom;
        public bool teRiRoom;
        public bool teReactRoom;
        public bool teShieldRoom;
        public bool teTestRoom;

    public bool Shields;
        public bool frontShield;
        public bool leftShield;
        public bool rightShield;
        public bool rearShield;
        public bool ShieldRepair;


        //if Getcomponenet<vrinput>().clicked == true then return pressed as true
    [SerializeField]
    private Material m_NormalMaterial;
    //[SerializeField]
    //private VRInteractiveItem m_InteractiveItem; //grabs the item
    [SerializeField]
    private Material m_OverMaterial; //set item material
    [SerializeField]
    private Renderer m_Renderer; //renders out (May need to delete cause of 3D object)
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private AudioSource click;

    void Start()
    {
            anim = GetComponent<Animator>();
            Pressed = false;
    }

private void OnEnable()
    {//to subscribe to an even you must use a previous function
            //m_InteractiveItem.OnOver += HandleOver;
            //m_InteractiveItem.OnOut += HandleOut;
            //m_InteractiveItem.OnClick += HandleClick;
    }

    private void OnDisable()
    {
            //m_InteractiveItem.OnOver -= HandleOver;
            //m_InteractiveItem.OnOut -= HandleOut;
            //m_InteractiveItem.OnClick -= HandleClick;
    }

    private void HandleOver()
    {
            Debug.Log("Show over state");
            //m_Renderer.material = m_OverMaterial;
    }

    private void HandleOut()
    {
            Debug.Log("Show out state");
            //m_Renderer.material = m_NormalMaterial;
    }

    private void HandleClick()
    {
            Debug.Log("Show click state");
            Pressed = true;
            
            Debug.Log("ButtonPressed " + Pressed);
           // anim.SetBool("Pressed", true);
            //anim.SetBool("Pressed", false);
            StartCoroutine(PressedWait());
            RoomFindClick();
           
    }
    
     void OnTriggerEnter(Collider other)
    {
          if(other.tag == "VrController")
          {
              Pressed = true;
              Debug.Log("It triggered VR controller" + Pressed);
              StartCoroutine(PressedWait());
              RoomFindClick();
          }
          Debug.Log("It was entered");
    }

    void RoomFindClick()
    {
       if (LifeSupport == true)
       {
          if (lGreenButton == true)
            {
                    LifeSupportRoom.GreenPressed = true;
            }
          else if(lRedButton == true)
            {
                  LifeSupportRoom.RedPressed = true;
            }
          else if(lBlueButton == true)
            {
                LifeSupportRoom.BluePressed = true;
            }
        }
       else if(LeftEngine == true)
        {
            if(leGreenButton == true)
            {
                LeftEngineRoom.GreenPressed = true;
            }
            else if(leRedButton == true)
            {
                LeftEngineRoom.RedPressed = true;
            }
            else if(leBlueButton == true)
            {
                LeftEngineRoom.BluePressed = true;
            }
        }
       else if(RightEngine)
        {
            if(reGreenButton == true)
            {
                RightEngineRoom.GreenPressed = true;
            }
            else if(reRedButton == true)
            {
                RightEngineRoom.RedPressed = true;
            }
            else if(reBlueButton == true)
            {
                RightEngineRoom.BluePressed = true;
            }
        }
       else if(ShieldRm == true)
        {
            if(sGreenButton == true)
            {
                ShieldRoom.GreenPressed = true;
            }
            else if(sRedButton == true)
            {
                ShieldRoom.RedPressed = true;
            }
            else if(sBluebutton == true)
            {
                ShieldRoom.BluePressed = true;
            }
        }
       else if(ReactorRm == true)
        {
            if(rrGreenButton == true)
            {
                ReactorRoom.GreenPressed = true;
            }
            else if(rrRedButton == true)
            {
                ReactorRoom.RedPressed = true;
            }
            else if(rrBlueButton == true)
            {
                ReactorRoom.BluePressed = true;
            }
        }
       //else if(Shields)//sets active shield
       // {
       //     if(frontShield)
       //     {
       //         ShieldMotor.frontActive = true;
       //         ShieldMotor.rightActive = false;
       //         ShieldMotor.leftActive = false;
       //         ShieldMotor.rearActive = false;
       //     }
       //     else if(rightShield)
       //     {
       //         ShieldMotor.rightActive = true;
       //         ShieldMotor.frontActive = false;
       //         ShieldMotor.leftActive = false;
       //         ShieldMotor.rearActive = false;
       //     }
       //     else if(leftShield)
       //     {
       //         ShieldMotor.leftActive = true;
       //         ShieldMotor.frontActive = false;
       //         ShieldMotor.rearActive = false;
       //         ShieldMotor.rightActive = false;
       //     }
       //     else if(rearShield)
       //     {
       //         ShieldMotor.rearActive = true;
       //         ShieldMotor.rightActive = false;
       //         ShieldMotor.frontActive = false;
       //         ShieldMotor.leftActive = false;
       //     }
       // }
       else if(Teleporter)//teleports to location
       {
           TeleportManager.tele = true;
           if(teLifeRoom)
           {
               
               TeleportManager.liGo = true;
               //set life room

           }
           else if(teLeRoom)
           {

               TeleportManager.leGo = true;
               //set left eng
           }
           else if(teRiRoom)
           {
               TeleportManager.riGo = true;
               //set right eng
           }
           else if(teReactRoom)
           {
               TeleportManager.reGo = true;
               //set reactor room
           }
           else if(teShieldRoom)
           {
               TeleportManager.shGo = true;
               //set shield room
           }
           else if(teTestRoom)
            {
                TeleportManager.testGo = true;
            }

       }
       else if(ShieldRepair)
        {
            //ShieldManager.AddShieldHealth(5f);
            //ShieldManager.CheckShieldHealth();
        }

    }

    IEnumerator PressedWait()
    {
            anim.SetBool("Pressed", true);
            yield return new WaitForSeconds(.1f);
            anim.SetBool("Pressed", false);
    }
      

  }
//}

