﻿using UnityEngine;
using System.Collections;

public class EleSetParent : MonoBehaviour {

    void OnCollisionEnter(Collision _other)
    {
        if (_other.collider.tag == "VrController")
        {
            _other.gameObject.transform.parent = gameObject.transform;

        }
    }

    void OnCollisionExit(Collision _other)
    {
        if (_other.collider.tag == "VrController")
        {
            _other.gameObject.transform.parent = null;

        }
    }
}
