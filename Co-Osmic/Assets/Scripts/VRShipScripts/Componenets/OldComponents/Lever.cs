﻿using UnityEngine;
//using VRStandardAssets.Utils;

public class Lever : MonoBehaviour {

    static private bool Up;
    

    public bool SetBegin;

    public bool LifeSupport;
        public bool lGreenLever;
        public bool lRedLever;
        public bool lBlueLever;

    public bool leftEng;
        public bool leGreenLever;
        public bool leRedLever;
        public bool leBlueLever;

    public bool rightEng;
        public bool riGreenLever;
        public bool riRedLever;
        public bool riBlueLever;

    public bool shieldRoom;
        public bool sGreenLever;
        public bool sRedLever;
        public bool sBlueLever;

    public bool reactorRm;
        public bool reGreenLever;
        public bool reRedLever;
        public bool reBlueLever;



    //[SerializeField]
    //private VRInteractiveItem m_InteractiveItem; //grabs the item
    [SerializeField]
    private Animator anim;

	
	void Start ()
    {
        anim = GetComponent<Animator>();
        Up = SetBegin;
        if (Up == true)
        {
            Up = false;
            anim.SetBool("Up", false);
        }
        else if (Up == false)
        {
            Up = true;
            anim.SetBool("Up", true);
        }
        CheckFindRoom();
	}

    private void OnEnable()
    {
        //m_InteractiveItem.OnOver += HandleOver;
        //m_InteractiveItem.OnOut += HandleOut;
        //m_InteractiveItem.OnClick += HandleClick;
    }

    private void OnDisable()
    {
        //m_InteractiveItem.OnOver -= HandleOver;
        //m_InteractiveItem.OnOut -= HandleOut;
        //m_InteractiveItem.OnClick -= HandleClick;
    }

    private void HandleOver()
    {
        Debug.Log("Show Over State Lever");
    }

    private void HandleOut()
    {
        Debug.Log("Show out state lever");
    }

    private void HandleClick()
    {
        Debug.Log("Show click state lever");
        if(Up == true)
        {
            Up = false;
            anim.SetBool("Up", false);
        }
        else if(Up == false)
        {
            Up = true;
            anim.SetBool("Up", true);
        }
        CheckFindRoom();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "VrController")
        {
            if(Up == true)
            {
                Up = false;
                anim.SetBool("Up", false);
            }
            else if(Up == false)
            {
                Up = true;
                anim.SetBool("Up", true);
            }
            CheckFindRoom();
        }
    }

    void CheckFindRoom()
    {
        if(LifeSupport == true)
        {
            if(lGreenLever == true)
            {
                if(Up == true)
                {
                    LifeSupportRoom.GreenLeverUp = true;
                }
                else if(Up == false)
                {
                    LifeSupportRoom.GreenLeverUp = false;
                }
            }
            else if(lBlueLever == true)
            {
                if (Up == true)
                {
                    LifeSupportRoom.BlueLeverUp = true;
                }
                else if (Up == false)
                {
                    LifeSupportRoom.BlueLeverUp = false;
                }
            }
            else if(lRedLever == true)
            {
                if (Up == true)
                {
                    LifeSupportRoom.RedLeverUp = true;
                }
                else if (Up == false)
                {
                    LifeSupportRoom.RedLeverUp = false;
                }
            }
        }
        else if(leftEng == true)
        {
            if(leGreenLever == true)
            {
                if (Up == true)
                {
                    LeftEngineRoom.GreenLeverUp = true;
                }
                else if (Up == false)
                {
                    LeftEngineRoom.GreenLeverUp = false;
                }
            }
            else if(leRedLever == true)
            {
                if (Up == true)
                {
                    LeftEngineRoom.RedLeverUp = true;
                }
                else if (Up == false)
                {
                    LeftEngineRoom.RedLeverUp = false;
                }
            }
            else if(leBlueLever == true)
            {
                if (Up == true)
                {
                    LeftEngineRoom.BlueLeverUp = true;
                }
                else if (Up == false)
                {
                    LeftEngineRoom.BlueLeverUp = false;
                }
            }
        }
        else if (rightEng == true)
        {
            if (riGreenLever == true)
            {
                if (Up == true)
                {
                    RightEngineRoom.GreenLeverUp = true;
                }
                else if (Up == false)
                {
                    RightEngineRoom.GreenLeverUp = false;
                }
            }
            else if (riRedLever == true)
            {
                if (Up == true)
                {
                    RightEngineRoom.RedLeverUp = true;
                }
                else if (Up == false)
                {
                    RightEngineRoom.RedLeverUp = false;
                }
            }
            else if (riBlueLever == true)
            {
                if (Up == true)
                {
                    RightEngineRoom.BlueLeverUp = true;
                }
                else if (Up == false)
                {
                    RightEngineRoom.BlueLeverUp = false;
                }
            }
        }
        else if (shieldRoom == true)
        {
            if (sGreenLever == true)
            {
                if (Up == true)
                {
                    ShieldRoom.GreenLeverUp = true;
                }
                else if (Up == false)
                {
                    ShieldRoom.GreenLeverUp = false;
                }
            }
            else if (sRedLever == true)
            {
                if (Up == true)
                {
                    ShieldRoom.RedLeverUp = true;
                }
                else if (Up == false)
                {
                    ShieldRoom.RedLeverUp = false;
                }
            }
            else if (sBlueLever == true)
            {
                if (Up == true)
                {
                    ShieldRoom.BlueLeverUp = true;
                }
                else if (Up == false)
                {
                    ShieldRoom.BlueLeverUp = false;
                }
            }
        }
        else if (reactorRm == true)
        {
            if (reGreenLever == true)
            {
                if (Up == true)
                {
                    ReactorRoom.GreenLeverUp = true;
                }
                else if (Up == false)
                {
                    ReactorRoom.GreenLeverUp = false;
                }
            }
            else if (reRedLever == true)
            {
                if (Up == true)
                {
                    ReactorRoom.RedLeverUp = true;
                }
                else if (Up == false)
                {
                    ReactorRoom.RedLeverUp = false;
                }
            }
            else if (reBlueLever == true)
            {
                if (Up == true)
                {
                    ReactorRoom.BlueLeverUp = true;
                }
                else if (Up == false)
                {
                    ReactorRoom.BlueLeverUp = false;
                }
            }
        }
    }


	// Update is called once per frame
	void Update () {
	
	}
}
