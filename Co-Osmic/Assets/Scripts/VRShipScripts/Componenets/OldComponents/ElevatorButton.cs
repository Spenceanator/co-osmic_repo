﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ElevatorButton : MonoBehaviour
{

    void OnTriggerEnter(Collider _other)
    {
        if(_other.tag == "VrController")
        {
            gameObject.GetComponent<Button>().onClick.Invoke();
            Debug.Log(gameObject + "Triggered");
			gameObject.GetComponent<Button>().image.color = Color.grey; //Show it is being activated
        }
        
    }

	private void OnTriggerExit(Collider _other)
	{
		if (_other.tag == "VrController")
		{
			gameObject.GetComponent<Button>().image.color = Color.white; //Show that the player is no longer interacting with the button
		}
	}
}