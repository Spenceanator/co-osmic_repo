﻿using UnityEngine;
using System.Collections;

public class TeleportManager : MonoBehaviour {

    public GameObject player;

    public Transform lifeTransporter;
    public Transform rightTransporter;
    public Transform leftTransporter;
    public Transform shieldTransporter;
    public Transform reactTransporter;
    public Transform testTransporter;

    //public Transform toPos;

    static public bool liGo;
    static public bool riGo;
    static public bool leGo;
    static public bool shGo;
    static public bool reGo;

    static public bool tele;

    static public bool testGo;

	
    void Update () 
    {
	    if(tele)
        {
            SetRoom();
            
        }
	}

    void SetRoom() //used to decide which room to transport to
    {
        tele = false;
        if(liGo)
        {
            riGo = false;
            leGo = false;
            shGo = false;
            reGo = false;
            testGo = false;
            
        }
        else if(riGo)
        {
            liGo = false;
            leGo = false;
            shGo = false;
            reGo = false;
            testGo = false;
        }
        else if(leGo)
        {
            liGo = false;
            riGo = false;
            shGo = false;
            reGo = false;
            testGo = false;
        }
        else if(shGo)
        {
            liGo = false;
            riGo = false;
            leGo = false;
            reGo = false;
            testGo = false;
        }
        else if(reGo)
        {
            liGo = false;
            riGo = false;
            leGo = false;
            shGo = false;
            testGo = false;
        }
        else if(testGo)
        {
            liGo = false;
            riGo = false;
            leGo = false;
            shGo = false;
            reGo = false;
        }
        else
        {
            Debug.Log("Error with false function");
        }
        ToRoom();
        
    }
    void ToRoom()
    {
        if(liGo)
        {
            player.transform.position = lifeTransporter.position;
            liGo = false;
        }
        else if(riGo)
        {
            player.transform.position = rightTransporter.position;
            riGo = false;
        }
        else if(leGo)
        {
            player.transform.position = leftTransporter.position;
            leGo = false;
        }
        else if(shGo)
        {
            player.transform.position = shieldTransporter.position;
            shGo = false;
        }
        else if(reGo)
        {
            player.transform.position = reactTransporter.position;
            reGo = false;
        }
        else if(testGo)
        {
            player.transform.position = testTransporter.position;
            testGo = false;
        }
        else
        {
            Debug.Log("Error with toroom function");
        }
    }
    
}
