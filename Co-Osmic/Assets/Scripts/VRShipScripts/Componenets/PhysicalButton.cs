﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightMessanger))]
public class PhysicalButton : MonoBehaviour {

/// <summary>
/// This is for configuration of the button
/// </summary>
#region Public Variables
	[Header("Axis:")]
	[SerializeField]
	private bool x;
	[SerializeField]
	private bool y;
	[SerializeField]
	private bool z;
	[Space(5)]
	[SerializeField]
	private float pressedFlt;
	[SerializeField]
	private float idleFlt;
	[Space(5)]
	[SerializeField]
	private AudioClip pressedSFX;
	[SerializeField]
	private AudioClip unpressedSFX;
	[Space(5)]
	[SerializeField]
	private bool usingTrigger;
	[SerializeField]
	private GameObject buttonObject;
	//Changes when user pressed a button (Accessed and checked by parent room)
#endregion

#region Private Variables
	private float activeAxis;
	private AudioSource myAudioSource;
	private bool pressing = false;
	private LightMessanger myLightMsngr;
#endregion

	void Awake()
	{
		myAudioSource = gameObject.GetComponent<AudioSource>();
		myLightMsngr = gameObject.GetComponent<LightMessanger>();
		if (usingTrigger)
		{
			gameObject.GetComponent<BoxCollider>().isTrigger = true;
		}
		if(buttonObject == null)
		{
			buttonObject = gameObject;
		}
	}
	
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		#region Update Axis
		if (x)
		{
			activeAxis = buttonObject.transform.localPosition.x;
				
		}
		else if (y)
		{
			activeAxis = buttonObject.transform.localPosition.y;
		}
		else if (z)
		{
			activeAxis = buttonObject.transform.localPosition.z;
		}
		else
		{
			Debug.LogError("Invalid axis selection on " + gameObject.name);
		}
		#endregion
		if (activeAxis <= pressedFlt)
		{
			Pressed();
		}
		if(activeAxis >= idleFlt)
		{
			UnPressed();
		}
	}

	public void SetGreenActive(bool _active)
	{
		myLightMsngr.SetGreenActive(_active);
	}

	private void Pressed()
	{
		if (!pressing)
		{
			myAudioSource.PlayOneShot(pressedSFX);
			pressing = true;
		}
		else
		{
			return;
		}

	}

	private void UnPressed()
	{
		if(pressing)
		{
			pressing = false;
			myAudioSource.PlayOneShot(unpressedSFX);
			if (myLightMsngr.CheckGreenActive())
				myLightMsngr.SetGreenActive(false);
			else
				myLightMsngr.SetGreenActive(true);

			
		}
		else
		{
			return;
		}
	}

	private void OnTriggerEnter(Collider _other)
	{
		float _pressedFlt = pressedFlt - 0.01f;
		float _idleFlt = idleFlt + 0.01f;

		if(!usingTrigger)
		{
			return;
		}
		if(_other.tag == "VrController")
		{
			#region Update Axis
			if (x)
			{
				//buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(_idleFlt, transform.localPosition.y, transform.localPosition.z), new Vector3(_pressedFlt, transform.localPosition.y, transform.localPosition.z), 2f * Time.deltaTime);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(_idleFlt, buttonObject.transform.localPosition.y, buttonObject.transform.localPosition.z), new Vector3(_pressedFlt, buttonObject.transform.localPosition.y, buttonObject.transform.localPosition.z), 2f);

			}
			else if (y)
			{
				//buttonObject.transform.localPosition = new Vector3(transform.localPosition.x, pressedFlt, transform.localPosition.z);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(buttonObject.transform.localPosition.x, _idleFlt, buttonObject.transform.localPosition.z), new Vector3(buttonObject.transform.localPosition.x, _pressedFlt, buttonObject.transform.localPosition.z), 2f);

			}
			else if (z)
			{
				//gameObject.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, pressedFlt);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(buttonObject.transform.localPosition.x, buttonObject.transform.localPosition.y, _idleFlt), new Vector3(buttonObject.transform.localPosition.x, buttonObject.transform.localPosition.y, _pressedFlt), 2f);
			}
			else
			{
				Debug.LogError("Invalid axis selection on " + gameObject.name);
			}
			#endregion
		}
	}

	private void OnTriggerExit(Collider _other)
	{
		float _pressedFlt = pressedFlt - 0.01f;
		float _idleFlt = idleFlt + 0.01f;

		if (!usingTrigger)
		{
			return;
		}
		if(_other.tag == "VrController")
		{
			#region Update Axis
			if (x)
			{
				//buttonObject.transform.localPosition = new Vector3(idleFlt, transform.localPosition.y, transform.localPosition.z);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(_pressedFlt, buttonObject.transform.localPosition.y, buttonObject.transform.localPosition.z), new Vector3(_idleFlt, buttonObject.transform.localPosition.y, buttonObject.transform.localPosition.z), 2f);


			}
			else if (y)
			{
				//buttonObject.transform.localPosition = new Vector3(transform.localPosition.x, idleFlt, transform.localPosition.z);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(buttonObject.transform.localPosition.x, _pressedFlt, buttonObject.transform.localPosition.z), new Vector3(buttonObject.transform.localPosition.x, _idleFlt, buttonObject.transform.localPosition.z), 2f);

			}
			else if (z)
			{
				//buttonObject.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, idleFlt);
				buttonObject.transform.localPosition = Vector3.Lerp(new Vector3(buttonObject.transform.localPosition.x, buttonObject.transform.localPosition.y, _pressedFlt), new Vector3(buttonObject.transform.localPosition.x, buttonObject.transform.localPosition.y, _idleFlt), 2f);

			}
			else
			{
				Debug.LogError("Invalid axis selection on " + gameObject.name);
			}
			#endregion
		}
	}

}
