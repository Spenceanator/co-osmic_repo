﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponenetLightController : MonoBehaviour {

	#region Public Variables
	[SerializeField]
	private GameObject componentObj;
	[SerializeField]
	private Material redMat;
	[SerializeField]
	private Material greenMat;
	#endregion

	#region Private Variables
	private LightMessanger myLightMsngr;
	//private PhysicalButton myButt;
	//private PhysicalValve myValve;
	//private PhysicalLever myLever;

	#endregion

	private void Awake()
	{
		myLightMsngr = componentObj.GetComponent<LightMessanger>();
		if(componentObj == null)
		{
			Debug.LogError("No componenet object on " + gameObject.name);
		}
		
	}

	private void OnEnable()
	{
		myLightMsngr.OnGreen += SetGreen;
		myLightMsngr.OnRed += SetRed;
	}

	void SetGreen()
	{
		//gameObject.GetComponent<Material>().color = greenMat;
		gameObject.GetComponent<MeshRenderer>().material = greenMat;
	}

	void SetRed()
	{
		//gameObject.GetComponent<Material>().color = redMat;
		gameObject.GetComponent<MeshRenderer>().material = redMat;
	}

	private void OnDisable()
	{
		myLightMsngr.OnGreen -= SetGreen;
		myLightMsngr.OnRed -= SetRed;
	}
}
