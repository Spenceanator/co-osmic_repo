﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is made to keep track of all the doors on the ship
/// Also this will lock doors and maintain which ones are locked
/// </summary>

public class DoorManager : MonoBehaviour {

	public List<GameObject> myDoors; //List for all the doors
	[SerializeField]
	private float WaitGiveDoorID = 1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	IEnumerator GiveDoorsIDs(float _wTime)
	{
		yield return new WaitForSeconds(_wTime);
		for (int i = 0; i <= myDoors.Count; i++)
		{
			myDoors[i].GetComponent<DoorOpenController>().doorID = i;
		}
	}

	/// <summary>
	/// Used to tell all the doors whether or not to automatically open
	/// without pressing the button on it's console
	/// </summary>
	/// <param name="_setter"></param>
	public void SetAllLocked(bool _setter)
	{
		for(int i = 0; i <= myDoors.Count; i++)
		{
			myDoors[i].GetComponent<DoorOpenController>().locked = _setter;
		}
	}

	/// <summary>
	/// Used to make the player force the door open
	/// will not open for player or enemy regardless.
	/// Also if _setter is true, will close doors with setHardLock
	/// </summary>
	/// <param name="_setter"></param>
	public void SetAllHardLocked(bool _setter)
	{
		for(int i = 0; i <= myDoors.Count; i++)
		{
			myDoors[i].GetComponent<DoorOpenController>().hardLocked = _setter;
			if(_setter)
			{
				myDoors[i].GetComponent<DoorOpenController>().ForceHardLock();
			}
		}
	}

	/// <summary>
	/// Randomaly will choose doors to soft lock
	/// to act as a possible malfunction
	/// </summary>
	public void SetRandomLockedDoors()
	{
		int _Iterations = Random.Range(0, myDoors.Count);
		for(int i = 0; i < _Iterations; i++)
		{
			int _setter = Random.Range(0, 1);
			int _doorChooser = Random.Range(0, myDoors.Count);
			if(_setter == 0)
			{
				myDoors[_doorChooser].GetComponent<DoorOpenController>().locked = true;
			}
			else if(_setter == 1)
			{
				myDoors[_doorChooser].GetComponent<DoorOpenController>().locked = false;
			}
		}
	}

	/// <summary>
	/// Randomly will choose and force close doors to hardlock
	/// to act as a possible malfunction
	/// </summary>
	public void SetRandomHardLockedDoors()
	{
		int _Iterations = Random.Range(0, myDoors.Count);
		for (int i = 0; i < _Iterations; i++)
		{
			int _setter = Random.Range(0, 1);
			int _doorChooser = Random.Range(0, myDoors.Count);
			if (_setter == 0)
			{
				myDoors[_doorChooser].GetComponent<DoorOpenController>().locked = true;
				myDoors[_doorChooser].GetComponent<DoorOpenController>().ForceHardLock();
			}
			else if (_setter == 1)
			{
				myDoors[_doorChooser].GetComponent<DoorOpenController>().locked = false;
			}
		}
	}

	/// <summary>
	/// Sets the specific door that is returned to locked or unlcoked depending on its current state.
	/// Can be used by either player when the need arises. Can only be used this way since at the beginning
	/// there is an IEnumerator giving each door an ID.
	/// </summary>
	/// <param name="_doorNum"></param>
	public void SetDoorLocked(int _doorNum) //DoorNUM is the doorID that is being searched for
	{
		for(int i = 0; i <= myDoors.Count; i++) //Do a quick search to find the door with that ID
		{
			if(myDoors[i].GetComponent<DoorOpenController>().doorID == _doorNum) //If the doorID and _doorNUM match
			{
				if (myDoors[i].GetComponent<DoorOpenController>().locked)
				{
					myDoors[i].GetComponent<DoorOpenController>().locked = false;
				}
				else if (!myDoors[i].GetComponent<DoorOpenController>().locked)
				{
					myDoors[i].GetComponent<DoorOpenController>().locked = true;
				}
				break;
			}
			if(_doorNum > myDoors.Count) //Just in case the player enters a number too high
			{
				break; //Break loop immediately instead of going through it multiple times
			}
		}
	}

	/// <summary>
	/// Similar to SetDoorLocked. Will search for the corrisponding door and hardlock it.
	/// Can only be used since there is an IEnumerator setting an ID for each door
	/// </summary>
	/// <param name="_doorNum"></param>
	public void SetDoorHardLocked(int _doorNum) //DoorNUM is the doorID that is being searched for
	{
		for (int i = 0; i <= myDoors.Count; i++) //Do a quick search to find the door with that ID
		{
			if (myDoors[i].GetComponent<DoorOpenController>().doorID == _doorNum) //If the doorID and _doorNUM match
			{
				if (myDoors[i].GetComponent<DoorOpenController>().hardLocked)
				{
					myDoors[i].GetComponent<DoorOpenController>().hardLocked = false;
				}
				else if (!myDoors[i].GetComponent<DoorOpenController>().hardLocked)
				{
					myDoors[i].GetComponent<DoorOpenController>().ForceHardLock();
				}
				break;
			}
			if(_doorNum > myDoors.Count) //Just in case the player enters a number too high
			{
				break; //Break loop immediately instead of going through it multiple times
			}
		}
	}



}
