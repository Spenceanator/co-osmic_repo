﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is unfinished;
/// </summary>

/* TODO:
 * Tell both command consoles which door ID it is
 * 
 * Have a wait to interact with both consoles
 * 
 * Door status available on console - Added needs to be implemented with door design
 * 
 * Create basic in game console (call scripts like Doors.lockDoor (_door id))
 * Console could be used to spawn things as well then implement voice later on
 * Have a public list of prefabs and give each prefab an ID script, so that way
 * You can straight up call that ID or have it read to you if you forget
 * 
 * Do some testing
 * 
 */

public class DoorOpenController : MonoBehaviour {

	public Animator anim;
	//[SerializeField]
	//private float distanceToOpen = 5f;
	//[SerializeField]
	//private float waitDistanceCheck;
	//private GameObject OpenForTarget;
	private int characterNearbyHash = Animator.StringToHash("character_nearby");

	private bool canOpenEnemys;
	private bool canOpenPlayer;
	public int inTrigger = 0;

	public string playerTag;
	public string enemyTag;

	public bool locked; //locking down the doors in case of intruders
	public bool hardLocked; //Needs to be opened manually by player to open will be handled in emergency script
	public bool stayOpen; //If locked and opened using UI will stay open when player leaves

	private DoorManager myDoorManager; //Reference to the door manager to add this door to the list

	private GameObject doorConsole;
	private GameObject secondaryDoorConsole;
	private ConsoleController primaryConsoleCntrl;
	private ConsoleController secondaryConsoleCntrl;

	/// <summary>
	/// This ID is set in the GUI and
	/// Monitor view so that while communication is happening
	/// between both players they can either soft lock or hard lock doors.
	/// ID is set in the manager. 
	/// </summary>
	public int doorID;

	// Use this for initialization
	void Awake()
	{
		anim = GetComponent<Animator>();
		myDoorManager = FindObjectOfType<DoorManager>();
		myDoorManager.myDoors.Add(gameObject);

		doorConsole = transform.GetChild(3).gameObject;
		primaryConsoleCntrl = doorConsole.GetComponent<ConsoleController>();
		primaryConsoleCntrl.doorNum.text = GetDoorNumber();
		primaryConsoleCntrl.doorStatus.text = GetDoorStatus();
		if(transform.childCount >= 4)
		{
			secondaryDoorConsole = transform.GetChild(4).gameObject;
			secondaryConsoleCntrl = secondaryDoorConsole.GetComponent<ConsoleController>();
			secondaryConsoleCntrl.doorNum.text = GetDoorNumber();
			secondaryConsoleCntrl.doorStatus.text = GetDoorStatus();
		}
		
	}

	// Update is called once per frame
	void Update()
	{

		//var distance = Vector3.Distance(transform.position, OpenForTarget.transform.position);

		//if(distanceToOpen >= distance)
		//{
		//	anim.SetBool(characterNearbyHash, true);
		//}
		//else
		//{
		//	anim.SetBool(characterNearbyHash, false);
		//}
	}

	private void FixedUpdate()
	{
		if(inTrigger >= 1) //Handles when people/enemies are in here
		{
			if(!locked && !hardLocked)
			{
				DoorOpen(true); //Opens door
			}
		}
		else if(inTrigger <= 0) //Handles when people/enemies leave
		{
			if(!hardLocked)
			{
				if (stayOpen)
					return;
				DoorOpen(false); //Closes door
			}
		}
	}

	/// <summary>
	/// Sets the state of the door (true) for open and (false) for close
	/// </summary>
	/// <param name="_open"></param>
	public void DoorOpen(bool _open)
	{
		anim.SetBool(characterNearbyHash, _open);
	}

	/// <summary>
	/// Should be called by a button to toggle opening the door or not
	/// </summary>
	public void DoorOpenToggle()
	{
		if(anim.GetBool(characterNearbyHash)) //If true close
		{
			anim.SetBool(characterNearbyHash, false);
		}
		else if(!anim.GetBool(characterNearbyHash)) //IF false open
		{
			anim.SetBool(characterNearbyHash, true);
		}
	}

	/// <summary>
	/// Called by a button to toggle locking the door
	/// Mainly for use only by Admin systems for testing purposes
	/// </summary>
	public void DoorLockToggle()
	{
		if(!locked)
		{
			locked = true;
		}
		else if(locked)
		{
			locked = false;
		}
		SetStatus();
	}

	/// <summary>
	/// Called by a button to hard lock the door
	/// Use for debugging reasons only; Should not be used in prod
	/// </summary>
	public void HardLockToggle()
	{
		if(!hardLocked)
		{
			hardLocked = true;
			ForceHardLock();
		}
		else if(hardLocked)
		{
			hardLocked = false;
		}
		SetStatus();
	}

	/// <summary>
	/// Will force the doors to close
	/// and is called by the door manager or 
	/// if this door needs to be hardlocked
	/// </summary>
	public void ForceHardLock()
	{
		DoorOpen(false);
		hardLocked = true;
	}

	public string GetDoorNumber()
	{
		string _id = doorID.ToString();
		return _id;
	}

	/// <summary>
	/// Gets the door status and returns a string.
	/// </summary>
	/// <returns></returns>
	public string GetDoorStatus()
	{
		if(hardLocked)
		{
			return "HardLocked";
		}
		else if(!hardLocked && locked)
		{
			return "Locked";
		}
		else
		{
			return "Standby";
		}
	}

	/// <summary>
	/// Sets the door status.
	/// </summary>
	public void SetStatus()
	{
		primaryConsoleCntrl.doorStatus.text = GetDoorStatus();
		if (secondaryConsoleCntrl != null)
			secondaryConsoleCntrl.doorStatus.text = GetDoorStatus();
	}

	//private void OnTriggerEnter(Collider other)
	//{
	//	if (other.tag == playerTag || other.tag == enemyTag)
	//	{
	//		inTrigger++;
	//	}
	//	if (other.tag == playerTag)
	//	{
	//		if (locked && !hardLocked)
	//		{
	//			anim.SetBool(characterNearbyHash, true);
	//		}
	//		else if (!locked && !hardLocked)
	//		{
	//			anim.SetBool(characterNearbyHash, true);
	//		}
	//	}
	//	else if (other.tag == enemyTag)
	//	{
	//		if (!locked || !hardLocked)
	//		{
	//			anim.SetBool(characterNearbyHash, true);
	//		}
	//	}
	//	//else
	//	//{
	//	//	anim.SetBool(characterNearbyHash, false);
	//	//}
	//}
	//private void OnTriggerExit(Collider other)
	//{
	//	if (other.tag == playerTag || other.tag == enemyTag)
	//	{
	//		inTrigger--;
	//	}
	//	if (inTrigger == 0)
	//	{

	//	}
	//}



}
