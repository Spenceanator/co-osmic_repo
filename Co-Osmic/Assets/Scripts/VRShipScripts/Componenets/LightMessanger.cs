﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightMessanger : MonoBehaviour {

	private bool greenActive = false;

	public delegate void GreenAction();
	public event GreenAction OnGreen;
	public delegate void RedAction();
	public event RedAction OnRed;

	public void SetGreenActive(bool _active)
	{
		greenActive = _active;
		if(_active)
		{
			OnGreen();
		}
		else if(!_active)
		{
			OnRed();
		}
	}

	public bool CheckGreenActive()
	{
		return greenActive;
	}

}
