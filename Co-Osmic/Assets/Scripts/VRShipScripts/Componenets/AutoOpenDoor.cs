﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoOpenDoor : MonoBehaviour {

	private DoorOpenController myDoorController;

	private void Awake()
	{
		myDoorController = gameObject.GetComponentInParent<DoorOpenController>();
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == myDoorController.playerTag || other.tag == myDoorController.enemyTag)
		{
			myDoorController.inTrigger++;
		}
	}
	private void OnTriggerExit(Collider other)
	{
		if(other.tag == myDoorController.playerTag || other.tag == myDoorController.enemyTag)
		{
			myDoorController.inTrigger--;
		}
	}

}
