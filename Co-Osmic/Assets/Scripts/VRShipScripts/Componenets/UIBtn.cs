﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIBtn : MonoBehaviour {

	[SerializeField]
	private Sprite pressedSprite;
	private Sprite currentSprite;
	private bool changeSprite;

	private void Awake()
	{
		{
			//If no sprite is entered ignore this
			if(pressedSprite == null)
			{
				return;
			}
			//If sprite is entered use sprite instead of changing color
			if(pressedSprite != null)
			{
				changeSprite = true;
				currentSprite = gameObject.GetComponent<Image>().sprite;
			}
		}
	}

	void OnTriggerEnter(Collider _other)
	{
		if (_other.tag == "VrController")
		{
			gameObject.GetComponent<Button>().onClick.Invoke();
			Debug.Log(gameObject + "Triggered");
			if (changeSprite)
			{
				gameObject.GetComponent<Image>().sprite = pressedSprite;
			}
			else
			{
				gameObject.GetComponent<Button>().image.color = Color.grey; //Show it is being activated
			}
		}

	}

	private void OnTriggerExit(Collider _other)
	{
		if (_other.tag == "VrController")
		{
			if(changeSprite)
			{
				gameObject.GetComponent<Image>().sprite = currentSprite;
			}
			else
			{
				gameObject.GetComponent<Button>().image.color = Color.white; //Show that the player is no longer interacting with the button
			}
		}
	}
}
