﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script is unfinished;
/// </summary>

/* Function:
 * This script will controller behaviours for each console.
 * 
 * Intended use:
 * Door manager sets consoles to display certain UI when game is in certain states.
 * Each console can be changed from standby, locked, and hardlocked.
 * 
 * TODO:
 * Get door num and status from door controller;
 * 
 * Stretch:
 * Change UI based on whether in debug or full release;
 * Give hackable option on each door to access debug or full release;
 */

public class ConsoleController : MonoBehaviour {

	//Parent door controller script
	//[SerializeField]
	//private GameObject parentDoor;

#region Variables set from other scripts

	public Text doorNum; //Set from parent door controller
	public Text doorStatus; //Set from parent door controller

#endregion


	/// <summary>
	/// Text game objects used to display the current status of the door.  (Set by door open controller)
	/// </summary>
	public GameObject doorNumTxt; //Set in editor
	public GameObject doorStatusTxt;//Set in editor

	

	void Awake () {
		//Set text variables-----------------------------------------------
		doorNum = doorNumTxt.GetComponent<Text>();
		doorStatus = doorStatusTxt.GetComponent<Text>(); //-----------------

		//Set parent door variable-------------------------------------------

	}
	
	// Update is called once per frame
	void Update () {
		//doorStatus
	}
}
