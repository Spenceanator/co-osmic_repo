﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LightMessanger))]
public class PhysicalLever : MonoBehaviour {

	#region Public Variables
	[Header("Movement Style")]
	[SerializeField]
	private bool positionTracking;
	[SerializeField]
	private bool rotationTracking;
	[Space(5)]
	[Header("Axis:")]
	[SerializeField]
	private bool x;
	[SerializeField]
	private bool y;
	[SerializeField]
	private bool z;
	[Space(5)]
	[SerializeField]
	private float currentMov;
	[Space(5)]
	[SerializeField]
	private float topRange; //top pos or rot for detecting lever
	[SerializeField]
	private float botRange; //bot pos or rot for detecting lever
	[SerializeField]
	private float buffer; //buffer to allow lever to not be at exact positions (+ or - from botRange and topRange)
	
	#endregion

	#region Private Variables
	private RandomSound myRandSound;
	private float locLastFrame;
	private bool inTop;
	private bool inBot;
	private bool goalTop = true; //Set for the goal of the lever to be
	private LightMessanger myLightMsngr;
	#endregion

	private void Awake()
	{
		myLightMsngr = gameObject.GetComponent<LightMessanger>();
	}

	void FixedUpdate()
	{
		if(positionTracking)
		{
			if (x)
			{
				currentMov = transform.localPosition.x;

			}
			else if (y)
			{
				currentMov = transform.localPosition.y;
			}
			else if (z)
			{
				currentMov = transform.localPosition.z;
			}
			else
			{
				Debug.LogError("Invalid axis selection on " + gameObject.name);
			}
		}
		else if(rotationTracking)
		{
			if (x)
			{
				currentMov = transform.localEulerAngles.x;
				currentMov = (currentMov > 180) ? currentMov - 360 : currentMov;

			}
			else if (y)
			{
				currentMov = transform.localEulerAngles.y;
			}
			else if (z)
			{
				currentMov = transform.localEulerAngles.z;
			}
			else
			{
				Debug.LogError("Invalid axis selection on " + gameObject.name);
			}
		}
		else
		{
			Debug.LogError("Invalid tracking selection on " + gameObject.name);
		}

		if (currentMov != locLastFrame)
		{
			CheckLocation();
		}

		//if (!greenActive && !newRange) //Checks if needs to set new range and does
		//{
		//	SetFixThreshold();
		//}
		//if (currentRotation != rotLastFrame)
		//{
		//	CheckRange();
		//	CheckPlaySound();
		//	Debug.LogWarning("Last rotation thing " + rotLastFrame);
		//}
		//rotLastFrame = currentRotation;
		locLastFrame = currentMov;
	}

	/// <summary>
	/// Checking where lever is currently at
	/// top  is topRange - buffer
	/// but is butRange + buffer
	/// </summary>
	private void CheckLocation()
	{

		float _topBuff = topRange - buffer;
		float _botBuff = botRange + buffer;

		if(currentMov >= _topBuff)
		{
			inTop = true;
		}
		else if(currentMov < _topBuff)
		{
			inTop = false;
		}

		if(currentMov <= _botBuff)
		{
			inBot = true;
		}
		else if( currentMov > _botBuff)
		{
			inBot = false;
		}

		if (goalTop && inTop)
		{
			SetGreenActivity(true, false);
		}
		else if (goalTop && !inTop)
		{
			SetGreenActivity(false, false);
		}
		else if (!goalTop && inBot)
		{
			SetGreenActivity(true, false);
		}
		else if (!goalTop && !inBot)
		{
			SetGreenActivity(false, false);
		}

	}

	
	private void SetGreenActivity(bool _active, bool _setGoal)
	{
		if(_active)
		{
			if (inTop)
			{
				myLightMsngr.SetGreenActive(true);
			}
			else if(inBot)
			{
				myLightMsngr.SetGreenActive(true);
			}
		}
		else if(!_active)
		{
			myLightMsngr.SetGreenActive(false);
		}
		if(_setGoal)
		{
			SetGoal();
		}
	}

	/// <summary>
	/// Called by father script to change location
	/// </summary>
	public void SetGoal()
	{
		if(goalTop)
		{
			goalTop = false;
		}
		else if(!goalTop)
		{
			goalTop = true;
		}
	}

	
	
}
