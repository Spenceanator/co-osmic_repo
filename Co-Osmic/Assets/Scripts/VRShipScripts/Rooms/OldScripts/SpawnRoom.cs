﻿using UnityEngine;
using System.Collections;

public class SpawnRoom : MonoBehaviour {

    public GameObject GameIsGoing;
    public GameObject playingBox;


	// Use this for initialization
	void Start () {

        playingBox.GetComponent<Renderer>().material.color = Color.red;
	
	}
	
	// Update is called once per frame
	void Update () {

        if (LevelManager.buttonColor == false)
        {
            playingBox.GetComponent<Renderer>().material.color = Color.red;
        }
        else if (LevelManager.buttonColor == true)
        {
            playingBox.GetComponent<Renderer>().material.color = Color.green;
        }
        	
	}
}
