﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeSupportRoom : MonoBehaviour {
    
    public GameObject buttonGreen;
    public GameObject buttonRed;
    public GameObject buttonBlue;
    public GameObject leverGreen;
    public GameObject leverBlue;
    public GameObject leverRed;
    public GameObject emerLight;
    public GameObject emerLight2;
    public GameObject emerLight3;

    //public Button buttGreen;

    public float lightIntensity;
    public float lowLightIntensity =2f;

    static public bool RoomDamaged;
    static public bool GreenPressed;
    static public bool RedPressed;
    static public bool BluePressed;

    static public bool GreenLeverUp;
    static public bool BlueLeverUp;
    static public bool RedLeverUp;

    static public bool roomFixed;

    [SerializeField]
    private AudioSource buttonW1;
    [SerializeField]
    private AudioSource buttonW2;
    [SerializeField]
    private AudioSource buttonW3;
    [SerializeField]
    private AudioSource Siren;
    [SerializeField]
    private AudioSource leverW1;
    [SerializeField]
    private AudioSource leverW2;
    [SerializeField]
    private AudioSource leverW3;
    [SerializeField]
    private AudioSource failSound;

    public Text text;

	void Start ()
    {
        roomFixed = true;
        ResetRoomRepair();
        text.color = Color.green;
	}
	
	void Update ()
    {
        if (RoomDamaged == true)
        {
           StartRepair();
           //StartCoroutine(LightFlash());
           emerLight.GetComponent<Light>().enabled = true;
            emerLight2.GetComponent<Light>().enabled = true;
            emerLight3.GetComponent<Light>().enabled = true;
            text.color = Color.red;
        }
        else if(!RoomDamaged)
        {
           emerLight.GetComponent<Light>().enabled = false;
            emerLight2.GetComponent<Light>().enabled = false;
            emerLight3.GetComponent<Light>().enabled = false;
            text.color = Color.green;
        }
	}

    void StartRepair()
    {
        roomFixed = false;
        if (GreenPressed == true)
        {
            //play sound
            if (RedPressed == true)
            {
                //play another sound
                if (GreenLeverUp == true)
                {
                    //play another fancy sound
                    if (BlueLeverUp == false)
                    {
                        //play a real fancy sound
                        if (BluePressed == true)
                        {
                            //so really extremly fancy sound
                            if (RedLeverUp == true)
                            {
                                //the best win sound ever in the world that has ever exhisted
                                roomFixed = true;
                                ResetRoomRepair();
                            }
                        }
                    }
                }
            }
            else if (BluePressed == true)
            {
                ResetRoomRepair();
            }
        }
        else if (RedPressed == true || BluePressed == true && GreenPressed == false)
        {
            ResetRoomRepair();
        }
    }
    //IEnumerator LightFlash()
    //{   
    //    yield return new WaitForSeconds(2);
    //    if (emerLight.GetComponent<Light>().intensity == lightIntensity)
    //    {
    //        emerLight.GetComponent<Light>().intensity = lowLightIntensity;
    //    }
    //    else
    //    {
    //        emerLight.GetComponent<Light>().intensity = lightIntensity;
    //    }
    //}

    void ResetRoomRepair()
    {
        GreenPressed = false;
        BluePressed = false;
        RedPressed = false;
        //don't reset levers as they are more physical than repeatea
        if (roomFixed == false)
        {
            roomFixed = false;
            RoomDamaged = true;
        }
        else if(roomFixed == true)
        {
            RoomDamaged = false;
        }
        Debug.Log("Room got reset");
        //play room fail sound
    }
    
}
