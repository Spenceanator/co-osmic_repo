﻿using UnityEngine;
using UnityEngine.UI;

public class WaveDisplayCounter : MonoBehaviour {

    Text text;

    void Start()
    {
        text = GetComponent<Text>();
        WaveManager.ResetAll();
    }

    void Update()
    {
        text.text = "" + WaveManager.waveCount;
    }
	
}
