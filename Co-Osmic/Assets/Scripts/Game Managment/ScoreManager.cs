﻿using UnityEngine;
using UnityEngine.UI;


public class ScoreManager : MonoBehaviour {

    static public int score;
    static public int newHighScore;
    

    Text text;

    void Start()
    {
        text = GetComponent<Text>();
        score = 0;
    }

    void Update()
    {
        if (score < 0)
            score = 0;

        text.text = "" + score;
    }

    static public void AddScore(int _AddAmount)
    {
        score += _AddAmount;
    }
    static public void SubScore(int _SubAmount)
    {
        score -= _SubAmount;
    }
    static public void ResetScore()
    {
        score = 0;
    }
    static public void SetHighScore()
    {
        newHighScore = score;
        int oldHighScore = PlayerPrefs.GetInt("highScore", 0);
        if (newHighScore > oldHighScore)
        {
            PlayerPrefs.SetInt("highscore", newHighScore);
            PlayerPrefs.Save();
        }
        //HighScore = score;
    }
}
