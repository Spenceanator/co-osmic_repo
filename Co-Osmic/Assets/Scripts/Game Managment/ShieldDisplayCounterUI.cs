﻿using UnityEngine;
using UnityEngine.UI;

public class ShieldDisplayCounterUI : MonoBehaviour
{

    // Use this for initialization
    Text text;
    [SerializeField]
    private GameObject shieldManObj;


    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {
        text.text = "" + shieldManObj.GetComponent<ShieldManager>().shieldHealth;

    }
}
