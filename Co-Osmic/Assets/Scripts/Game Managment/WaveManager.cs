﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour {

    static public int aSpawned;
    static public int eSpawned;

    static public int waveCount;

    static public int aDestroyed;
    static public int eDestroyed;

    static public bool WaveOver;

    void Start()
    {
        //WaveOver = true;
    }
    
   static public void CheckWaveOver()
    {
       if(aSpawned == aDestroyed && eSpawned == eDestroyed)
       {
           WaveOver = true;
           Debug.Log("Wave Over");
           ResetAstroidSpawn();
           ResetEnemySpawn();
           //WaveOver = false;
       }
       else if(aSpawned != aDestroyed && eSpawned != eDestroyed)
       {
           WaveOver = false;
       }
    }

    static public void AddWave(int _wave)
    {
        waveCount += _wave;
    }
    static public void addAstroidSpawn(int _spawned)
    {
        aSpawned += _spawned;
    }
    static public void addEnemySpawn(int _spawned)
    {
        eSpawned += _spawned;
        Debug.Log("Created Enemies: " + eSpawned + "..." + "Destroyed Enemies: " + eDestroyed);
    }
    static public void addAstroidDestroy(int _destroyed)
    {
        aDestroyed += _destroyed;
    }
    static public void addEnemyDestroy(int _destroyed)
    {
        eDestroyed += _destroyed;
        Debug.Log("Created Enemies: " + eSpawned + "..." + "Destroyed Enemies: " + eDestroyed);
    }

    static public void ResetAstroidSpawn()
    {
        aSpawned = 0;
        aDestroyed = 0;
    }
    static public void ResetEnemySpawn()
    {
        eSpawned = 0;
        eDestroyed = 0;
    }
    static public void ResetWaveCount()
    {
        waveCount = 0;
    }


    static public void ResetAll()
    {
        ResetEnemySpawn();
        ResetAstroidSpawn();
        ResetWaveCount();
    }
}
