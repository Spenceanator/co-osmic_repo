﻿using UnityEngine;
using UnityEngine.VR;


public class CamMirrorOff : MonoBehaviour {

    //public variable for use change in game
    public bool on = false;

	// Use this for initialization
    void Start()
    {
        VRSettings.showDeviceView = on;
    }
   
}
