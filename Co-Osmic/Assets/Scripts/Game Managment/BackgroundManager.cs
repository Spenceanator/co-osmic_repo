﻿using UnityEngine;
using System.Collections;

public class BackgroundManager : MonoBehaviour {

    public Texture[] textures;
    public int currentTexture;
    public Renderer rend;
	// Use this for initialization
	void Start ()
    {
        rend = GetComponent<Renderer>();

        //Random randy = new Random();
        if (WaveManager.WaveOver == true)
        {
            currentTexture++;
            currentTexture %= textures.Length;
            rend.material.mainTexture = textures[currentTexture];
        }
	}
	
	// Update is called once per frame
	void Update()
    {

    }
}
