﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManagerObjectPooling : MonoBehaviour {

    public GameObject lAstroid;
    public GameObject mAstroid;
    public GameObject sAstroid;

    public GameObject EnemyBig;
    public GameObject EnemySmall;

    public Text PressP; //Start game
    public Text PressH; //Reset scene
    public Text ShieldRoomTitle; //ShieldRoom title
    public Text ShieldInstructions; //shield room repair instructions
    public Text ReactorRoomTitle; //Reactor Room Title
    public Text ReactorInstructions; //reactor repair instructions
    public Text LifeSupportTitle; //title
    public Text LifeInstructions;
    public Text leftEngineTitle;
    public Text leftEngineInstructions;
    public Text rightEngineTitle;
    public Text rightEngineInstructions;

    public Canvas Instructions;

    public Canvas noobInstructions;

    [SerializeField]
    private GameObject MonitorPoolerObj;

    MonitorObjectPooler MonitorPoolScript;

    public Transform enemySpawn;

    public int numOfRoids = 4;

    [SerializeField]
    private int waveCount;

    private bool waveSpawned;
    public bool willEnemSpawn;

    static public bool gameBeOver;

    public int instructionChooser;
    //public int pooledLAstroids = 10;
    //public int pooledMAstroids = 20;
    //public int pooledSAstroids = 40;

    //List<GameObject> largeAstroids;
    //List<GameObject> mediumAstroids;
    //List<GameObject> smallAstroids;

    public string LevelLoader;


    //wave 1 - 4 should spawn four roids
    //wave 5, will add one, then six add one, till 10 waves and 10 roids
    //Enemies will spawn more often

    //IEnumerator delay on spawning enemies (if enemy count = 0 then wait 2 seconds spawn enemy) (if enemy count = 1, 2

    static public bool buttonColor;

    [SerializeField]
    private bool AdminOn;

    void Awake()
    {
        WaveManager.WaveOver = false;
        waveSpawned = false;

        willEnemSpawn = false;
        gameBeOver = false;

        MonitorPoolScript = MonitorPoolerObj.GetComponent<MonitorObjectPooler>();
        //SpawnSmallEnemy();
        //SpawnBigEnemy();


        //SpawnBigEnemy();
        //SpawnSmallEnemy();
        //SpawnAstroid(numOfRoids);
        //SpawnWave(waveCount);
        //largeAstroids = new List<GameObject>();
        //for(int i = 0; i < pooledLAstroids; i++)
        //{
        //    GameObject objL = (GameObject)Instantiate(lAstroid);
        //    objL.SetActive(false);
        //    largeAstroids.Add(objL);
        //}
        //mediumAstroids = new List<GameObject>();
        //for(int j = 0; j < pooledMAstroids; j++)
        //{
        //    GameObject objM = (GameObject)Instantiate(mAstroid);
        //    objM.SetActive(false);
        //    mediumAstroids.Add(objM);
        //}
        //smallAstroids = new List<GameObject>();
        //for(int k = 0; k < pooledSAstroids; k++)
        //{
        //    GameObject objS = (GameObject)Instantiate(sAstroid);
        //    objS.SetActive(false);
        //    smallAstroids.Add(objS);
        //}
        PressP.enabled = true;
        PressH.enabled = false;
        ShieldRoomTitle.enabled = true;
        ShieldInstructions.enabled = true;
        ReactorRoomTitle.enabled = false;
        ReactorInstructions.enabled = false;
        LifeSupportTitle.enabled = false;
        LifeInstructions.enabled = false;
        leftEngineTitle.enabled = false;
        leftEngineInstructions.enabled = false;
        rightEngineTitle.enabled = false;
        rightEngineInstructions.enabled = false;

        Instructions.enabled = false;
        noobInstructions.enabled = true;
        instructionChooser = 0;
        buttonColor = false;
    }
    //static void enable medium astroid
    //static public void mediumEnable(Transform _position)
    //{
    //    Transform _pos = _position;

    //}
    //static void enable small astroid
    void Update()
    {

        if (gameBeOver)
        {
            GameOver();
            PressH.enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!waveSpawned)
            {
                PressP.enabled = false;
                //willEnemSpawn = true;
                waveSpawned = true;
                WaveManager.WaveOver = true;
                noobInstructions.enabled = false;
                StartCoroutine(waitStartEnemySpawn());
                buttonColor = true; 
            }

        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (Instructions.enabled == false)
            {
                Instructions.enabled = true;
            }
            else if (Instructions.enabled == true)
            {
                Instructions.enabled = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            instructionChooser++;
            ShowInstructions();

        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            instructionChooser--;
            ShowInstructions();
        }

        if (AdminOn)
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                SpawnBigEnemy();
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                SpawnSmallEnemy();
            }
        }

        if (WaveManager.WaveOver == true)
        {
            if (WaveManager.waveCount >= 0 && WaveManager.waveCount <= 4)
            {
                SpawnWave(4);
                //WaveManager.waveCount++;
                //waveCount++;
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
                waveSpawned = true;
                if (WaveManager.waveCount >= 2 && WaveManager.waveCount <= 4)
                {
                    //spawn 1 enemy
                }
            }
            else if (WaveManager.waveCount == 5)
            {
                SpawnWave(5);
                //WaveManager.waveCount++;
                WaveManager.AddWave(1);
                //waveCount++;
                WaveManager.WaveOver = false;
            }
            else if (WaveManager.waveCount == 6)
            {
                SpawnWave(6);
                //WaveManager.waveCount++;
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
            }
            else if (WaveManager.waveCount == 7)
            {
                SpawnWave(7);
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
            }
            else if (WaveManager.waveCount == 8)
            {
                SpawnWave(8);
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
            }
            else if (WaveManager.waveCount == 9)
            {
                SpawnWave(9);
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
            }
            else if (WaveManager.waveCount >= 10)
            {
                SpawnWave(10);
                WaveManager.AddWave(1);
                WaveManager.WaveOver = false;
            }
            else
            {
                Debug.Log(WaveManager.waveCount + " " + "Error");
            }

        }
    }

    void GameOver()
    {
        //ShipController.canMove = false;
        //willEnemSpawn = false;
        if (Input.GetKeyDown(KeyCode.H))
        {
            SceneManager.LoadScene(LevelLoader);
        }
        gameBeOver = false;
    }

    void ShowInstructions()
    {
        if (instructionChooser == 0)
        {//enable shield room instructions
            ShieldRoomTitle.enabled = true;
            ShieldInstructions.enabled = true;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = false;
            rightEngineInstructions.enabled = false;
        }
        else if (instructionChooser == 1)
        {//reactor room
            ShieldRoomTitle.enabled = false;
            ShieldInstructions.enabled = false;

            ReactorRoomTitle.enabled = true;
            ReactorInstructions.enabled = true;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = false;
            rightEngineInstructions.enabled = false;
        }
        else if (instructionChooser == 2)
        {//life room
            ShieldRoomTitle.enabled = false;
            ShieldInstructions.enabled = false;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = true;
            LifeInstructions.enabled = true;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = false;
            rightEngineInstructions.enabled = false;
        }
        else if (instructionChooser == 3)
        {//left engine
            ShieldRoomTitle.enabled = false;
            ShieldInstructions.enabled = false;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = true;
            leftEngineInstructions.enabled = true;

            rightEngineTitle.enabled = false;
            rightEngineInstructions.enabled = false;
        }
        else if (instructionChooser == 4)
        {
            ShieldRoomTitle.enabled = false;
            ShieldInstructions.enabled = false;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = true;
            rightEngineInstructions.enabled = true;
        }
        else
        {
            Debug.Log("Instruction chooser  error");
        }
        if (instructionChooser < 0)
        {
            instructionChooser = 4;
            ShieldRoomTitle.enabled = false;
            ShieldInstructions.enabled = false;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = true;
            rightEngineInstructions.enabled = true;
        }
        else if (instructionChooser > 4)
        {
            instructionChooser = 0;
            ShieldRoomTitle.enabled = true;
            ShieldInstructions.enabled = true;

            ReactorRoomTitle.enabled = false;
            ReactorInstructions.enabled = false;

            LifeSupportTitle.enabled = false;
            LifeInstructions.enabled = false;

            leftEngineTitle.enabled = false;
            leftEngineInstructions.enabled = false;

            rightEngineTitle.enabled = false;
            rightEngineInstructions.enabled = false;


        }
    }

    void SpawnWave(int _waveCount)
    {

        WaveManager.AddWave(waveCount);
        if (_waveCount <= 4)
        {
            SpawnAstroid(4);
        }
        else if (_waveCount == 5)
        {
            SpawnAstroid(5);
            //waitBigEnemSpawn();
        }
        else if (_waveCount == 6)
        {
            SpawnAstroid(6);
        }
        else if (_waveCount == 7)
        {
            SpawnAstroid(7);
        }
        else if (_waveCount == 8)
        {
            SpawnAstroid(8);
        }
        else if (_waveCount == 9)
        {
            SpawnAstroid(9);
        }
        else if (_waveCount >= 10)
        {
            SpawnAstroid(10);
        }
        else
        {
            Debug.Log("SpawnWave Function Error");
        }
    }
    IEnumerator waitBigEnemSpawn()
    {
        yield return new WaitForSeconds(Random.Range(20, 50));
        SpawnBigEnemy();
    }

    IEnumerator waitSmallEnemSpawn()
    {
        yield return new WaitForSeconds(Random.Range(20, 50));
        SpawnSmallEnemy();
    }

    IEnumerator waitStartEnemySpawn()
    {
        int _spawnWait;
        int _whichEnemy;
        //Debug.Log("Running ienumerator");
        while (willEnemSpawn)
        {
            _spawnWait = Random.Range(20, 90);
            _whichEnemy = Random.Range(0, 2);
            if (WaveManager.waveCount > 5 && WaveManager.waveCount <= 10)
                _spawnWait -= 10;
            else if (WaveManager.waveCount > 10 && WaveManager.waveCount <= 15)
                _spawnWait -= 15;
            else if (WaveManager.waveCount > 15)
                _spawnWait -= 20;
            Debug.Log(_spawnWait);
            Debug.Log("Which Enemy: " + _whichEnemy + " || 0 is big, 1 is small");
            yield return new WaitForSeconds(_spawnWait);

            if (_whichEnemy == 0)
            {
                SpawnBigEnemy();
            }
            else if (_whichEnemy == 1)
            {
                SpawnSmallEnemy();
            }
        }
    }
    void SpawnBigEnemy()
    {
        //Transform _spawn;
        Quaternion _spawnRotation = Quaternion.identity;
        int randRightSpawn = Random.Range(1, 9);
        //choose to spawn on top right
        //if (randRightSpawn <= 3)
        //{
        //    enemySpawn = p3;
        //}
        ////choose to spawn on middle
        //else if (randRightSpawn >= 4 && randRightSpawn <= 6)
        //{
        //    enemySpawn = p4;
        //}
        ////choose to spawn bottom left (p5)
        //else if (randRightSpawn >= 7 && randRightSpawn <= 9)
        //{
        //    enemySpawn = p5;
        //}
        //_spawn = p4;
        Instantiate(EnemyBig, enemySpawn.position, _spawnRotation);

        //EnemyBig.SetActive(true);
    }

    void SpawnSmallEnemy()
    {
        int randRightSpawn = Random.Range(1, 9);
        //choose to spawn on top right
        //if (randRightSpawn <= 3)
        //{
        //    enemySpawn = p3;
        //}
        ////choose to spawn on middle
        //else if (randRightSpawn >= 4 && randRightSpawn <= 6)
        //{
        //    enemySpawn = p4;
        //}
        ////choose to spawn bottom left (p5)
        //else if (randRightSpawn >= 7 && randRightSpawn <= 9)
        //{
        //    enemySpawn = p5;
        //}
        //Transform _spawn;
        Quaternion _spawnRotation = Quaternion.identity;
        //_spawn = p4;
        Instantiate(EnemySmall, enemySpawn.position, _spawnRotation);

    }

    void SpawnAstroid(int _roids)
    {
        MonitorPoolScript.SetActiveObjs(MonitorPoolScript.LargeAsteroidObjects, MonitorPoolScript.largeAsteroid, _roids, null, null);
        //for (int i = _roids; i >= 1; i--)
        //{
        //    var randSpawn = Random.Range(1, 8);
        //    Transform spawn;
        //    Quaternion spawnRotation = Quaternion.identity;
        //    if (randSpawn == 1)
        //    {
        //        spawn = p1;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
       
        //    }
        //    else if (randSpawn == 2)
        //    {
        //        spawn = p2;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
       
        //    }
        //    else if (randSpawn == 3)
        //    {
        //        spawn = p3;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
         
        //    }
        //    else if (randSpawn == 4)
        //    {
        //        spawn = p4;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
            
        //    }
        //    else if (randSpawn == 5)
        //    {
        //        spawn = p5;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
             
        //    }
        //    else if (randSpawn == 6)
        //    {
        //        spawn = p6;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
                
        //    }
        //    else if (randSpawn == 7)
        //    {
        //        spawn = p7;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
               
        //    }
        //    else if (randSpawn == 8)
        //    {
        //        spawn = p8;
        //        Instantiate(lAstroid, spawn.position, spawnRotation);
        //    }
        //}
    }
}
