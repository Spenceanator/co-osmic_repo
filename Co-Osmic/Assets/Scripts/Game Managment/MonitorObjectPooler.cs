﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonitorObjectPooler : MonoBehaviour {

    
    public GameObject largeAsteroid;
    [SerializeField]
    private int largeAsteroidAmount;

    
    public GameObject mediumAsteroid;
    [SerializeField]
    private int mediumAsteroidAmount;


    public GameObject smallAsteroid;
    [SerializeField]
    private int smallAsteroidAmount;


    public GameObject smallEnemy;
    [SerializeField]
    private int smallEnemyAmount;

    public GameObject smallEnemyProjectile;
    [SerializeField]
    private int smallEnemyProjectileAmount;

    public GameObject largeEnemy;
    [SerializeField]
    private int largeEnemyAmount;

    public GameObject largeEnemyProjectile;
    [SerializeField]
    private int largeEnemyProjectileAmount;

    public GameObject playerProjectile;
    [SerializeField]
    private int playerProjectileAmount;

    //player laser
    //player shield pulse

    public List<GameObject> LargeAsteroidObjects;
    public List<GameObject> MediumAsteroidObjects;
    public List<GameObject> SmallAsteroidObjects;
    public List<GameObject> SmallEnemyObjects;
    public List<GameObject> SmallProjectileObjects;
    public List<GameObject> LargeEnemyObjects;
    public List<GameObject> LargeProjectileObjects;
    public List<GameObject> PlayerProjectileObjects;

    // Use this for initialization
    void Start()
    {
        LargeAsteroidObjects = new List<GameObject>();
        MediumAsteroidObjects = new List<GameObject>();
        SmallAsteroidObjects = new List<GameObject>();
        SmallEnemyObjects = new List<GameObject>();
        SmallProjectileObjects = new List<GameObject>();
        LargeAsteroidObjects = new List<GameObject>();
        LargeProjectileObjects = new List<GameObject>();
        PlayerProjectileObjects = new List<GameObject>();



        
        InstantiateGameObjects(LargeAsteroidObjects, largeAsteroid, largeAsteroidAmount);
        InstantiateGameObjects(MediumAsteroidObjects, mediumAsteroid, mediumAsteroidAmount);
        InstantiateGameObjects(SmallAsteroidObjects, smallAsteroid, smallAsteroidAmount);
        InstantiateGameObjects(SmallEnemyObjects, smallEnemy, smallEnemyAmount);
        InstantiateGameObjects(SmallProjectileObjects, smallEnemyProjectile, smallEnemyProjectileAmount);
        InstantiateGameObjects(LargeAsteroidObjects, largeEnemy, largeEnemyAmount);
        InstantiateGameObjects(LargeProjectileObjects, largeEnemyProjectile, largeEnemyProjectileAmount);
        InstantiateGameObjects(PlayerProjectileObjects, playerProjectile, playerProjectileAmount);

    }
    /*
    //Function to add a gameObject onto the list if everything is active and needs another one
    //Add this in before prod
        public GameObject GetPooledObject()
        {
            for (int i = 0; i < pooledObjects.Count; i++)
            {
                if (!pooledObjects[i].activeInHierarchy)
                {
                    return pooledObjects[i];
                }
            }

            GameObject obj = (GameObject)Instantiate(pooledObject);
            obj.SetActive(false);
            pooledObjects.Add(obj);
            return obj;

        }
    */

    public void SetActiveObjs(List<GameObject> _list, GameObject _currentObj, int _iterations, Transform _enablePosition, Transform _enableRotation)
    {
        int AmountEnabled = 0;
        int AlreadyEnabled = 0;
        for (int x = 0; x < _list.Count; x++)//Checking to see if object is enabled if disabled enable it and break for loop
        {
            if (!_list[x].activeInHierarchy)
            {
                _list[x].gameObject.SetActive(true);
                AmountEnabled++;
                if(_enablePosition != null)
                {
                    _list[x].gameObject.transform.position = _enablePosition.position;
                }
                if(_enableRotation != null)
                {
                    _list[x].gameObject.transform.rotation = _enablePosition.rotation;
                }
                
            }
            else if(_list[x].activeInHierarchy)
            {
                AlreadyEnabled++;
            }
            if(AmountEnabled == _iterations || AmountEnabled >= _iterations)
            {
               return;
            }
        }
        if (_iterations > _list.Count || AmountEnabled + _iterations > _list.Count) //just in case more objects are needed than already instantiated
        {
            int dif;
            int exDif;
            if(_iterations > _list.Count)
            {
                dif = _iterations - _list.Count;
                for (int y = 0; y < dif; y++)
                {
                    GameObject obj = (GameObject)Instantiate(_currentObj);
                    obj.SetActive(true);
                    _list.Add(obj);
                }
            }
            else if(AmountEnabled + _iterations > _list.Count)
            {
                exDif = AmountEnabled + _iterations;
                exDif -= _list.Count;
                for(int z = 0; z < exDif; z++)
                {
                    GameObject obj = (GameObject)Instantiate(_currentObj);
                    obj.SetActive(true);
                    _list.Add(obj);
                }
            }
        }
        
    }

    void InstantiateGameObjects(List<GameObject> _list, GameObject _currentObj, int _loopNum)
    {
        for(int i = 0; i < _loopNum; i++)
        {
            GameObject obj = (GameObject)Instantiate(_currentObj);
            obj.SetActive(false);
            _list.Add(obj);
        }
    }

}
