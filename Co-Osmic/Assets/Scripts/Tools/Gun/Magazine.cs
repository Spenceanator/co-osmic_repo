﻿using UnityEngine;
using System.Collections;

public class Magazine : MonoBehaviour {


    //private List<int> Bullets = new List<int>();
    //private int[] Bullets = new int[30];
    [SerializeField]
    private int Bullets;

    public int currentBullets;

   
    private bool inGun;

	// Use this for initialization
	void Awake () {

        Bullets = 30;
        currentBullets = Bullets;
        inGun = false;
	
	}
	
    
	// Update is called once per frame
	void Update () {

        if (inGun == true)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = false;
        }
        else if(inGun == false)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
        }

    }

    public int BulletDel(int _int)
    {
        currentBullets -= _int;
        return currentBullets;
    }

    public void AttachedToGun(bool _ingun)
    {
        inGun = _ingun;  
    }
}
