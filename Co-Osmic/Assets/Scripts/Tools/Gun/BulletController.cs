﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {
    [SerializeField]
    private float moveSpeed = 10;


    private bool triggered;
    // Use this for initialization
    void Start()
    {
        triggered = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * moveSpeed;

        if (triggered == true)
        {
            Destroy(gameObject);
        }

    }

    void OnCollisionEnter(Collision _other)
    {
        if(!triggered)
        {
            if(_other.collider.tag != "pGun")
            {
                Debug.Log("LAZERZ HITTING " + _other.gameObject);
                Destroy(gameObject);
                triggered = true;
            }
            
        }
       

    }
}
