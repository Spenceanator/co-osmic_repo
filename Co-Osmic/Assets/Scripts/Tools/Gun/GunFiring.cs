﻿using UnityEngine;
using System.Collections;
namespace VRTK
{
    public class GunFiring : MonoBehaviour {

        [SerializeField]
        private GameObject laser;
        [SerializeField]
        private Transform firePoint;

        [SerializeField]
        private Animator anim;
         
        private int availableBullets;

        [SerializeField]
        private GameObject magSnap;

        [SerializeField]
        private float trigSwitch = 0.2f;
        private GameObject attachedMagazine;

        private bool hasFired;

        // Use this for initialization
        void Start()
        {
            hasFired = false;
            gameObject.GetComponent<VRTK_InteractableObject>().isUsable = false;
        }

        // Update is called once per frame
        void Update()
        {
            //if (magSnap.GetComponent<VRTK_SnapDropZone>().isSnapped == true)
            //{
            //    attachedMagazine = magSnap.transform.GetChild(1).gameObject;
            //    availableBullets = attachedMagazine.GetComponent<Magazine>().currentBullets;
            //    attachedMagazine.GetComponent<Magazine>().AttachedToGun(true);
            //    if (availableBullets > 0)
            //    {
            //        gameObject.GetComponent<VRTK_InteractableObject>().isUsable = true;
            //    }
            //    else if (availableBullets <= 0)
            //    {
            //        gameObject.GetComponent<VRTK_InteractableObject>().isUsable = false;
            //    }

            //}

            //else if (magSnap.GetComponent<VRTK_SnapDropZone>().isSnapped == false)
            //{
            //    if (attachedMagazine != null)
            //    {
            //        attachedMagazine.GetComponent<Magazine>().AttachedToGun(false);
            //        attachedMagazine = null;
            //    }
            //    availableBullets = 0;

            //}

            if (gameObject.GetComponent<VRTK_InteractableObject>().IsUsing() == true)
            {
                Fire();
            }
            else if(gameObject.GetComponent<VRTK_InteractableObject>().IsUsing() == false)
            {
                hasFired = false;
            }
            
        }

        IEnumerator TriggerAnim(float _trigTime)
        {
            //set trigger to one position
            yield return new WaitForSeconds(_trigTime);
            //set trigger to original position
        }

        //fire selector using ienumrator for shooting

        private void Fire()
        {
            StartCoroutine(TriggerAnim(trigSwitch));
            if(availableBullets > 0)
            {
                if (hasFired == false)
                {
                    FireLaser();
                    attachedMagazine.GetComponent<Magazine>().BulletDel(1);
                    hasFired = true;
                }
            }
            else if(availableBullets <= 0)
            {
                //play clunk sound
                
                
            }
        }

        private void FireLaser()
        {
            //play fire sound
            Instantiate(laser, firePoint.position, firePoint.rotation);


        }
    }
}
