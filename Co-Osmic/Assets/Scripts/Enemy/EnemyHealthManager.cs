﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour {

    public int currentHealth;
     [SerializeField]
    private int startHealth;

    void Awake()
    {

    }

    void OnEnable()
    {
        ResetHealth();
    }

    public void AddHealth(int _health)
    {
        currentHealth += _health;
        CheckHealth();
    }
    public void lowerHealth(int _health)
    {
        currentHealth -= _health;
        CheckHealth();
    }
    public void ResetHealth() //enter reset amount
    {
        currentHealth = startHealth;
    }
    public void NewMaxHealth(int _health)
    {
        startHealth = _health;
        currentHealth = startHealth;
    }

    public void CheckHealth()
    {
        if(currentHealth <= 0)
        {
            //gameObject.SetActive(false); //enemy is dead (play explosion particles)
        }
        else if(currentHealth > startHealth)
        {
            ResetHealth();
        }
    }

    void OnDisable()
    {
        ResetHealth();
    }
}
