﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldHit : MonoBehaviour {

 

    public float wTime = 2f;

    //public bool isHit;
    public bool canHit; //is set to false in enemyshieldmotor

    //private GameObject player;
    private EnemyShieldManager myShieldManager;
    private BlinkHit myBlinker;
    private EnemyShieldMotor myShieldMotor;
    private EnemyShipController myShipController;

    public int damageTaken;

    private int checkIterations = 0; //For doing the test loop on shield checking

    void Awake()
    {
        canHit = true;
        // player = transform.parent.gameObject;
        myShieldManager = transform.parent.GetComponent<EnemyShieldManager>();
        myBlinker = transform.parent.GetComponent<BlinkHit>();
        myShieldMotor = transform.parent.GetComponent<EnemyShieldMotor>();
        myShipController = transform.parent.GetComponent<EnemyShipController>();
    }

    void Update()
    {
       
    }

    void ApplyDamage(int _damage)
    {
        if(myShieldManager.shieldHealth >= 0)
        {
            myShieldManager.LowerShieldHealth(_damage);
        }
        else if (myShieldManager.shieldHealth < 0)
        {
            myShieldManager.shieldHealth = 0;
        }
    }

    void OnCollisionEnter(Collision _collision)
    {
        Debug.Log("Enemy shield hit log");
        if(canHit)
        {
            canHit = false;
            Debug.Log("Shield hit triggered");
            //StartCoroutine(setCollision(wTime));
            //minus shield health on any hit
            ApplyDamage(damageTaken);
            //if (myShieldManager.shieldHealth >= 0)
            //    myShieldManager.LowerShieldHealth(10);
            //goes less that zero just make it zero
            //if (myShieldManager.shieldHealth < 0)
            //    myShieldManager.shieldHealth = 0;
            myShieldManager.CheckShieldHealth();
            //isHit = true;
            myShieldMotor.shieldHit = true;

            myBlinker.shieldsBlinking = true;

            StartCoroutine(myShieldMotor.SetCollision(wTime));
            

            //if (_collider.tag == "EnemyProjectile" || _collider.tag == "SmallEnemyProjectile")
            //{
            //    _collider.gameObject.SetActive(false);
            //    //Destroy(_collider.gameObject);
            //}
        }
    }
    void OnTriggerEnter(Collider _collider)
    {
        Debug.Log("Enemy shield trigger hit log");
        if (canHit)
        {
            canHit = false;
            Debug.Log("Shield hit triggered");
            //StartCoroutine(setCollision(wTime));
            //minus shield health on any hit
            ApplyDamage(damageTaken);
            //if (myShieldManager.shieldHealth >= 0)
            //    myShieldManager.LowerShieldHealth(10);
            //goes less that zero just make it zero
            //if (myShieldManager.shieldHealth < 0)
            //    myShieldManager.shieldHealth = 0;
            myShieldManager.CheckShieldHealth();
            //isHit = true;
            myShieldMotor.shieldHit = true;

            myBlinker.shieldsBlinking = true;

            StartCoroutine(myShieldMotor.SetCollision(wTime));


            //if (_collider.tag == "EnemyProjectile" || _collider.tag == "SmallEnemyProjectile")
            //{
            //    _collider.gameObject.SetActive(false);
            //    //Destroy(_collider.gameObject);
            //}
        }
    }

    public void TriggerHit()
    {
        Debug.Log("Enemy shield trigger hit log");
        if (canHit)
        {
            canHit = false;
            Debug.Log("Shield hit triggered");
            //StartCoroutine(setCollision(wTime));
            //minus shield health on any hit
            ApplyDamage(damageTaken);
            //if (myShieldManager.shieldHealth >= 0)
            //    myShieldManager.LowerShieldHealth(10);
            //goes less that zero just make it zero
            //if (myShieldManager.shieldHealth < 0)
            //    myShieldManager.shieldHealth = 0;
            myShieldManager.CheckShieldHealth();
            //isHit = true;
            myShieldMotor.shieldHit = true;

            StartCoroutine(myShieldMotor.SetCollision(wTime));
            myBlinker.shieldsBlinking = true;
            //StartCoroutine(ColliderCheck(wTime));
        }
    }
   
    IEnumerator ColliderCheck(float _time)
    {
        checkIterations++;
        yield return new WaitForSeconds(_time + .1f);
        if(canHit)
        {
            GetComponent<Collider>().enabled = true;
            checkIterations = 0;
        }
        else
        {
            StartCoroutine(ColliderCheck(_time / 2f));
        }
        if(checkIterations > 4)
        {
            gameObject.SetActive(false);
            
        }
    }

    //private int disCount;
    //private void OnDisable() //resets for next spawn
    //{
        //disCount++;
        //canHit = true;
        //GetComponent<Collider>().enabled = true;
        //Debug.Log(gameObject + "Disable count: " + disCount);
    //}
}
