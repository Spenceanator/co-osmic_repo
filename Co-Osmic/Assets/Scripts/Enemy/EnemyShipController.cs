﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyHealthManager))]
[RequireComponent(typeof(EnemyShipWander))]
[RequireComponent(typeof(EnemyShipFire))]
public class EnemyShipController : MonoBehaviour {


	public float speed = 10f;

	[SerializeField]
	private float accelerationForce = 10f;
	[SerializeField]
	private float rotationForce = 3f;

	//[SerializeField]
	//private Transform firePoint1;
	//[SerializeField]
	//private Transform firePoint2;
	//static public bool isFired;
	//small ship
	// static public bool smallShip;

	//beginning variable to tell whether it just started or not
	//public bool begin;
	//public bool beginTwo;
	[SerializeField]
	private float flySpeed = 200f;
	private float turnSpeed = 1f; //Managed by turn function
	[SerializeField]
	private float moveTimer = 1f;
	[SerializeField]
	private float turnTimer = 5f;

	public GameObject warpEffect;

	public GameObject enemProjectile;
	[SerializeField]
	private GameObject firedProjectile1;
	[SerializeField]
	private GameObject firedProjectile2;

	private Camera cam;
	private float leftConstraint;
	private float rightConstraint;
	private float bottomConstraint;
	private float topConstraint;
	private float distanceZ = 30.0f;
	[SerializeField]
	private float spawnBuffer = 0.5f;

	[SerializeField]
	private GameObject MonObjectPooler;
	MonitorObjectPooler monitorPooler;

	private EnemyShieldManager myShieldManager;
	private EnemyShieldMotor myShieldMotor;
	private EnemyHealthManager myHealth;
	private EnemyShipHit myHit; //Not a required component since it is required in a child object
	private EnemyShipWander myWander;

	public int scoreToAdd;

	//[SerializeField]
	//private GameObject frontShield;
	//[SerializeField]
	//private GameObject backShield;
	//[SerializeField]
	//private GameObject leftShield;
	//[SerializeField]
	//private GameObject rightShield;

	//Have a enemy shield manager
	public bool largeShip;
	public bool medShip;
	public bool smallShip;

	//public bool shipIsHit;
	public bool canHit;

	private int instCounter;
	private bool instCheck;
	private bool canMove;

	void Awake()
	{
		//lastPosition = transform.position;
		//upLastPos = transform.position;
		//bigEnemyobj.GetComponent<Rigidbody>().AddForce(bigEnemyobj.GetComponent<Rigidbody>().transform.right * speed * 20);
		//transform.position = Vector3.MoveTowards(transform.position, pointA.position, flySpeed);
		//currentPoint = pointA;
		//isFired = false;
	   
		myHealth = gameObject.GetComponent<EnemyHealthManager>();
		cam = Camera.main;
		distanceZ = Mathf.Abs(cam.transform.position.z + transform.position.z);
		leftConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).x;
		rightConstraint = -leftConstraint;
		bottomConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, distanceZ)).y;
		topConstraint = cam.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, distanceZ)).y;

		MonObjectPooler = GameObject.Find("MonitorObjectManager");
		monitorPooler = MonObjectPooler.GetComponent<MonitorObjectPooler>();
		myHit = GetComponentInChildren<EnemyShipHit>();
		myWander = GetComponent<EnemyShipWander>();

		//shipIsHit = false;
		canHit = false;
		instCounter = 0;
		instCheck = false;

		

		if(largeShip || medShip)
		{
			myShieldManager = gameObject.GetComponent<EnemyShieldManager>();
			myShieldMotor = gameObject.GetComponent<EnemyShieldMotor>();
		}
		else if(smallShip)
		{
			//firePoint2 = null; //small ships will only have 1 firepoint and move fairly quickly
		}

	}

	void OnEnable()
	{
		if(instCounter >= 1)
		{
			WaveManager.addEnemySpawn(1);
			instCheck = true;
		}
		instCounter++;
		canMove = true;
		RandomSpawn();

		//Turn();
	}

	void RandomSpawn()
	{
		int _Const = Random.Range(0, 3);
		float _topBotRandom = Random.Range(leftConstraint, rightConstraint);
		float _leftRightRandom = Random.Range(bottomConstraint, topConstraint);

		if (_Const == 0)
		{
			var rotation = new Vector3(0, 0, Random.Range(0, 180));
			transform.Rotate(rotation);
			transform.position = new Vector3(leftConstraint - spawnBuffer, _leftRightRandom, transform.position.z);
		}
		else if (_Const == 1)
		{
			var rotation = new Vector3(0, 0, Random.Range(180, 360));
			transform.Rotate(rotation);
			transform.position = new Vector3(rightConstraint + spawnBuffer, _leftRightRandom, transform.position.z);
		}
		else if (_Const == 2)
		{
			var rotation = new Vector3(0, 0, Random.Range(90, 270));
			transform.Rotate(rotation);
			transform.position = new Vector3(_topBotRandom, topConstraint + spawnBuffer, transform.position.z);
		}
		else if (_Const == 3)
		{
			var rotation = new Vector3(0, 0, Random.Range(-90, 90));
			transform.Rotate(rotation);
			transform.position = new Vector3(_topBotRandom, bottomConstraint - spawnBuffer, transform.position.z);
		}
	}

	// Update is called once per frame

	void Update()
	{

		//if (firedProjectile1 == null)
		//{
		//	FireProjetile(firePoint1);
		//}
		//if(firedProjectile2 == null)
		//{
		//	FireProjetile(firePoint2);
		//}

		if (canMove)
		{
			Move();
			//Turn();
		}
		//if (turning)
		//{
		//    GetComponent<Rigidbody>().AddTorque(0, 0, turnSpeed * rotationForce * accelerationForce * Time.deltaTime);
		//}

		//if(shipIsHit)
		//{
		//    canHit = false;
		//    GetComponent<Collider>().enabled = false;
		//}
		//else if(!shipIsHit)
		//{
		//    canHit = true;
		//    GetComponent<Collider>().enabled = true;
		//}

	}

	void Move()
	{
		GetComponent<Rigidbody>().AddForce(transform.up * flySpeed * accelerationForce * Time.deltaTime);
		//StartCoroutine(AddForce(moveTimer));
	}

	IEnumerator AddForce(float _time)
	{
		if(canMove)
		{
			GetComponent<Rigidbody>().AddForce(transform.up * flySpeed * accelerationForce * Time.deltaTime);
			yield return new WaitForSeconds(_time);
			//yield return new WaitForSeconds(_time * 20);
			StartCoroutine(AddForce(_time));
		}
	   
		 
	}

	private bool randTurnSet = false;
	private bool turning;
	void Turn()
	{
		StartCoroutine(AddTurn(turnTimer));   
	}


	IEnumerator AddTurn(float _time)
	{
		//if(canMove)
		//{
		if (!randTurnSet)
		{
			turnSpeed = Random.Range(-50, 50);
			randTurnSet = true;
		}

		//    GetComponent<Rigidbody>().AddTorque(0, 0, turnSpeed * rotationForce * accelerationForce * Time.deltaTime);
		//    yield return new WaitForSeconds(_time);
		//    randTurnSet = false;
		//    StartCoroutine(AddTurn(_time));
		//}
		turning = true;
		yield return new WaitForSeconds(_time);
		turning = false;

		StartCoroutine(AddTurn(_time));

		
		
	}

	public void GivePoints()
	{
		ScoreManager.AddScore(scoreToAdd);
		gameObject.SetActive(false);
	}

	//void OnTriggerEnter(Collider _collider)
	//{
	//    if(canHit)
	//    {
	//        ManageHit(_collider);
	//    }

	//}

	//void OnCollisionEnter(Collision _collision)
	//{
	//    if(canHit)
	//    {
	//        ManageHit(_collision.collider);
	//    }

	//}



	//void ManageHit(Collider _collider)
	//{
	//    if (_collider.tag == "PlayerProjectile")
	//    {
	//        hit(_collider);
	//        //ScoreManager.AddScore(scoreToAdd);
	//        //playerKill = true;

	//    }
	//    if (_collider.tag == "Shield" || _collider.tag == "mPlayer")
	//    {
	//        //StartCoroutine(ShieldCollideWait(sWaitCollide));
	//        hit(_collider);
	//    }
	//    if (_collider.tag == "Enemy")
	//    {
	//        hit(_collider);
	//    }

	//}

	//void hit(Collider _collider)
	//{
	//    //gameObject.SetActive(false);
	//    if(myHealth.currentHealth <= 0)
	//    {
	//        if(_collider.tag == "PlayerProjectile")
	//        {
	//            ScoreManager.AddScore(scoreToAdd);
	//        }
	//        else if(_collider.tag == "Shield" || _collider.tag == "mPlayer")
	//        {
	//            int halfScore;
	//            halfScore = scoreToAdd / 2;
	//            Mathf.Round(halfScore);
	//            ScoreManager.AddScore(halfScore);
	//        }
	//            gameObject.SetActive(false);

	//    }


	//    myHealth.CheckHealth();
	//}

	void FireProjetile(Transform _firePoint)
	{
		//var newProjectile = Instantiate(enemProjectile, firePoint.position, Quaternion.identity);
		//FiredProjectile = newProjectile;
		monitorPooler.SetActiveObjs(monitorPooler.LargeProjectileObjects, monitorPooler.largeEnemyProjectile, 1, _firePoint, null);
		//if(_firePoint == firePoint1)
		//{

		//}
		//else if(_firePoint == firePoint1)
		//{

		//}
		//else
		//{
		//	Debug.LogError(gameObject + " Fire Point not set");
		//}
	}

	void OnDisable()
	{
		myHealth.ResetHealth();
		if(instCheck)
		{
			WaveManager.CheckWaveOver();
		}
		
	}
}
