﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldManager : MonoBehaviour {

    [SerializeField]
    private bool adminOn;

    public int shieldHealth = 10;

    private EnemyShieldMotor myShieldMotor;

    void Awake()
    {
        shieldHealth = 10;
        myShieldMotor = gameObject.GetComponent<EnemyShieldMotor>();
    }

    public void AddShieldHealth(int _shieldHealth)
    {
        shieldHealth += _shieldHealth;
    }
    public void LowerShieldHealth(int _shieldHealth)
    {
        shieldHealth -= _shieldHealth;
    }
    public void ResetShieldHealth()
    {
        shieldHealth = 10;
    }

    public void CheckShieldHealth()
    {
        if (shieldHealth <= 10 && shieldHealth != 0) //Set start with one shield then change
        {
           //if(myShieldMotor.firstShieldReset)
           // {
           //     myShieldMotor.firstShieldEnabled = true;
           // }
           // myShieldMotor.shieldEnabled = true;
            myShieldMotor.allShieldsEnabled = true;

        }
        else if (shieldHealth <= 0)
        {
            myShieldMotor.allShieldsEnabled = false;
            myShieldMotor.firstShieldEnabled = true;
            myShieldMotor.firstShieldReset = true;//set reseter to false or true
        }
        else if (shieldHealth > 5)
        {
            myShieldMotor.shieldEnabled = false;
            myShieldMotor.firstShieldEnabled = false;
            myShieldMotor.allShieldsEnabled = true;

        }
        if (shieldHealth <= 5 && shieldHealth > 0)
        {
            if (myShieldMotor.firstShieldReset)
            {
                myShieldMotor.firstShieldEnabled = true;
            }
            myShieldMotor.shieldEnabled = true;
        }

    }

    private void OnDisable()
    {
        ResetShieldHealth();
    }
}
