﻿using UnityEngine;
using System.Collections;

public class EnemyProjectileController : MonoBehaviour {

    public float speed;

    public GameObject expdEffect;
    //public GameObject mediumAstroid;
    //public GameObject smallAstroid;

    public int damageToGive;

    //public float destroyDelayCounter = 3f;

    public Vector3 lastPosition;
    public float DistanceTraveled;
    public float maxDistance = 20f;

	/// <summary>
	/// Raycasting prediction range
	/// </summary>
	[SerializeField]
	private int predictionRange = 2;

	RaycastHit rayHit;

    void Awake()//awake is initiated before start
    {
        //Physics.IgnoreLayerCollision(12, 13, true);
        lastPosition = transform.position;
        //var rotation = new Vector3(0, 0, Random.Range(-360, 360));
        //transform.Rotate(rotation);
        //BigEnemyController.isFired = true;
    }

	private void OnEnable()
	{
		DistanceTraveled = 0;
		lastPosition = transform.position;


	}
	// Update is called once per frame
	void Update()
    {

		//GetComponent<Rigidbody>().AddForce(GetComponent<Rigidbody>().transform.up * speed * Time.deltaTime, ForceMode.VelocityChange);
		transform.position += transform.up * speed * Time.deltaTime;

		DistanceTraveled += Vector3.Distance(transform.position, lastPosition);
        if (DistanceTraveled >= maxDistance)
        {
            gameObject.SetActive(false);
            //Destroy(gameObject);
            //BigEnemyController.isFired = false;
        }


    }
    void OnTriggerEnter(Collider _collider)
    {
        if (_collider.tag == "Shield")
        {
            gameObject.SetActive(false);
            _collider.gameObject.GetComponent<HitShield>().damageToGive = damageToGive;
            //Destroy(gameObject);
            //BigEnemyController.isFired = false;
        }
        if (_collider.tag == "mPlayer")
        {
            gameObject.SetActive(false);
            //hurt player function
            //Destroy(gameObject);
            //BigEnemyController.isFired = false;
        }
        //if(_collider.tag == "LargeAstroid" || _collider.tag == "MediumAstroid" || _collider.tag == "SmallAstroid")
        //{
        //    BigEnemyController.isFired = false;
        //}
    }

	private void OnDisable()
	{
		GetComponent<Rigidbody>().AddForce(Vector3.zero);
	}
	//void OnDestroy()
	//{
	//    BigEnemyController.isFired = false;
	//}

	//void OnTriggerExit(Collider other)
	//{
	//    //other.GetComponent<
	//    //Physics.IgnoreCollision()
	//    if (other.tag == "LargeAstroid")
	//    {

	//        Debug.Log("Hit");
	//        //Instantiate(expdEffect, transform.position, transform.rotation);
	//        Destroy(gameObject);
	//        Destroy(other.gameObject);
	//        BigEnemyController.isFired = false;
	//        WaveManager.addAstroidDestroy(1);
	//        Instantiate(mediumAstroid, transform.position, Quaternion.identity);
	//        Instantiate(mediumAstroid, transform.position, Quaternion.identity);
	//        WaveManager.addAstroidSpawn(2);
	//        //StartCoroutine(meduiumAWait(medWait));
	//        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);

	//    }
	//    else if (other.tag == "MediumAstroid")
	//    {
	//        Debug.Log("MedHit");
	//        //Instantiate(expdEffect, transform.position, transform.rotation);
	//        Destroy(gameObject);
	//        WaveManager.addAstroidDestroy(1);
	//        Destroy(other.gameObject);
	//        BigEnemyController.isFired = false;
	//        Instantiate(smallAstroid, transform.position, Quaternion.identity);
	//        WaveManager.addAstroidSpawn(2);
	//        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
	//    }
	//    else if (other.tag == "SmallAstroid")
	//    {
	//        Debug.Log("SmallHit");
	//        //Instantiate(expdEffect, transform.position, transform.rotation);
	//        Destroy(gameObject);
	//        Destroy(other.gameObject);
	//        BigEnemyController.isFired = false;
	//        WaveManager.addAstroidDestroy(1);
	//        Debug.Log("Created: " + WaveManager.aSpawned + " .... " + "Destroyed: " + WaveManager.aDestroyed);
	//        WaveManager.CheckWaveOver();
	//    }

	//}
}
