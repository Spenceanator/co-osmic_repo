﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShieldMotor : MonoBehaviour {

    public bool shieldEnabled; //Just having one shield at a time
    public bool allShieldsEnabled; //absolute zero shields enabled
    public bool firstShieldEnabled; //set all shields to false except front ** Setter **
    public bool firstShieldReset; //reseter for all shields excpet front

    public GameObject frontShield;
    private EnemyShieldHit frontHitShield;
    public GameObject rightShield;
    private EnemyShieldHit rightHitShield;
    public GameObject leftShield;
    private EnemyShieldHit leftHitShield;
    public GameObject rearShield;
    private EnemyShieldHit rearHitShield;

    public bool frontActive;
    public bool leftActive;
    public bool rightActive;
    public bool rearActive;

    public float wColTime = 2f;

    public bool shieldHit;

    private EnemyShieldManager myShieldManager;
    private EnemyShipHit myShipHit;
    private EnemyShipController myShipController;

    void Awake()
    {
        myShipHit = gameObject.GetComponentInChildren<EnemyShipHit>();
        myShieldManager = gameObject.GetComponent<EnemyShieldManager>();

        frontHitShield = frontShield.GetComponent<EnemyShieldHit>();
        rightHitShield = rightShield.GetComponent<EnemyShieldHit>();
        leftHitShield = leftShield.GetComponent<EnemyShieldHit>();
        rearHitShield = rearShield.GetComponent<EnemyShieldHit>();

        myShipController = gameObject.GetComponent<EnemyShipController>();

        if(myShipController.largeShip)
        {
            firstShieldEnabled = false;
            shieldEnabled = false;
            allShieldsEnabled = true;
        }
        else if(myShipController.medShip)
        {
            firstShieldEnabled = true;
            shieldEnabled = true;
            allShieldsEnabled = true;
        }
        else if(myShipController.smallShip)
        {
            firstShieldEnabled = false;
            shieldEnabled = false;
            allShieldsEnabled = false;
        }

    }

    private void OnEnable()
    {
        if(myShipController.largeShip)
        {
            firstShieldEnabled = false;
            shieldEnabled = false;
            allShieldsEnabled = true;
        }
        else if(myShipController.medShip)
        {
            firstShieldEnabled = true;
            shieldEnabled = true;
            allShieldsEnabled = true;
        }
        else if(myShipController.smallShip)
        {
            firstShieldEnabled = false;
            shieldEnabled = false;
            allShieldsEnabled = false;
        }
    }

    void Update()
    {

        if (allShieldsEnabled)
        {
            if (firstShieldEnabled)
            {
                SetStartRemainShield();
                shieldEnabled = true;
            }
            if (shieldEnabled)
            {
                ShieldSetter();
            }
            else if (!shieldEnabled)
            {
                if (myShieldManager.shieldHealth >= 5)
                {
                    SetActivity(true, true, true, true);

                    firstShieldReset = true;
                }
                //else if(myShieldManager.shieldHealth < 5 && myShieldManager.shieldHealth > 0)
                //{
                //    firstShieldReset = true;
                //}
            }
        }
        else if (!allShieldsEnabled)
        {
            SetActivity(false, false, false, false);
        }
        //if (shieldHit)
        //{
        //    StartCoroutine(SetCollision(wColTime));
        //}

    }

    public IEnumerator SetCollision(float _w) //called from the hit shield
    {
        //ShipController.DoneBeenHit = true;
        //myShipHit.shipIsHit = true; //HitPlayer.shipIsHit = true; //Lets call the ship hit setcollision instead
        StartCoroutine(myShipHit.SetCollision(_w));
        //shieldHit = false;//HitShield.isHit = false;
        SetShieldCol(false);
        //Debug.Log("Shield colliders off");
        yield return new WaitForSeconds(_w);
        //Debug.Log("Shield colliders on");
        SetShieldCol(true);
        //HitShield.isHit = false;
        //ShipController.DoneBeenHit = false;
        //myShipHit.shipIsHit = false;//HitPlayer.shipIsHit = false;
        //HitShield.canHit = true;
        SetShieldCanHit(true);
        
        

    }

    void SetShieldCanHit(bool _sb)
    {
        frontHitShield.canHit = _sb;
        rearHitShield.canHit = _sb;
        leftHitShield.canHit = _sb;
        rightHitShield.canHit = _sb;

        frontHitShield.damageTaken = 0;
        rearHitShield.damageTaken = 0;
        leftHitShield.damageTaken = 0;
        rightHitShield.damageTaken = 0;
    }

    void SetActivity(bool _shield1, bool _shield2, bool _shield3, bool _shield4) //Set which shields become active
    {
        frontShield.SetActive(_shield1);
        rightShield.SetActive(_shield2);
        leftShield.SetActive(_shield3);
        rearShield.SetActive(_shield4);
    }

    void SetShieldCol(bool _c) //set c ollider for shields for when they or player gets hit
    {
        frontShield.GetComponent<Collider>().enabled = _c;
        rightShield.GetComponent<Collider>().enabled = _c;
        leftShield.GetComponent<Collider>().enabled = _c;
        rearShield.GetComponent<Collider>().enabled = _c;
    }

    void SetStartRemainShield() //set all sheilds to false except front when starting
    {

        frontShield.SetActive(true);
        rightShield.SetActive(false);
        leftShield.SetActive(false);
        rearShield.SetActive(false);
        Debug.Log("First Shield Enabled");
        //rightShield.GetComponent<Collider>().enabled = true;
        //leftShield.GetComponent<Collider>().enabled = true;
        //rearShield.GetComponent<Collider>().enabled = true;
        firstShieldEnabled = false;
        firstShieldReset = false;
        frontActive = true;
        //set frontshield true all else false

    }


    void ShieldSetter() //Sets the shield that you want active and disables the others
    {
        if (frontActive == true)
        {
            SetActivity(true, false, false, false);
            //rightActive = false;
            //leftActive = false;
            //rearActive = false;
        }
        else if (rightActive == true)
        {
            SetActivity(false, true, false, false);
            //frontActive = false;
            //leftActive = false;
            //rearActive = false;
        }
        else if (leftActive == true)
        {
            SetActivity(false, false, true, false);
            //frontActive = false;
            //rightActive = false;
            //rearActive = false;
        }
        else if (rearActive == true)
        {
            SetActivity(false, false, false, true);
            //frontActive = false;
            //rightActive = false;
            //leftActive = false;
        }


        //{
        //    if(Input.GetButtonDown("FrontShield"))
        //    {
        //        SetActivity(true, false, false, false);
        //    }
        //    else if(Input.GetButtonDown("RightShield"))
        //    {
        //        SetActivity(false, true, false, false);
        //    }
        //    else if(Input.GetButtonDown("LeftShield"))
        //    {
        //        SetActivity(false, false, true, false);
        //    }
        //    else if(Input.GetButtonDown("RearShield"))
        //    {
        //        SetActivity(false, false, false, true);
        //    }
    }

}
