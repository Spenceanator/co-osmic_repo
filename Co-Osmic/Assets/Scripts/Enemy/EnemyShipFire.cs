﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is meant to instantiate the correct amount of projectiles and disable them
/// before they are needed. The script needs to set these instantiated projectiles
/// as references under the firepoints that way they can be called after they are 
/// they are disabled.
/// </summary>

public class EnemyShipFire : MonoBehaviour {

	[SerializeField]
	private Transform firePoint1;
	[SerializeField]
	private Transform firePoint2;
	[SerializeField]
	private GameObject proj1;
	[SerializeField]
	private GameObject proj2;
	[SerializeField]
	private GameObject enemProjectile;


	private void Awake()
	{
		if(firePoint2 != null)
		{
			proj1 = Instantiate(enemProjectile, firePoint1.position, firePoint1.rotation);
			proj2 = Instantiate(enemProjectile, firePoint2.position, firePoint2.rotation);
			proj1.SetActive(false);
			proj2.SetActive(false);
		}
		else
		{
			proj1 = Instantiate(enemProjectile, firePoint1.position, firePoint1.rotation);
			proj1.SetActive(false);
		}
	}

	private void FireProjectile()
	{
		proj1.SetActive(true);
		proj1.transform.position = firePoint1.position;
		proj1.transform.rotation = firePoint1.rotation;
		if(firePoint2 != null)
		{
			proj2.SetActive(true);
			proj2.transform.position = firePoint2.position;
			proj2.transform.rotation = firePoint2.rotation;
		}
	}

	private void Update()
	{
		if(proj1.activeSelf == false && proj2.activeSelf == false)
		{
			FireProjectile();
		}
		if(proj1.activeSelf)
		{
			proj1.transform.position += GetComponent<Rigidbody>().velocity * Time.deltaTime;
		}
		if (proj2.activeSelf)
		{
			proj2.transform.position += GetComponent<Rigidbody>().velocity * Time.deltaTime;
		}
	}
}
