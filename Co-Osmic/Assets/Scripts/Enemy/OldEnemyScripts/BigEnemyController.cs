﻿using UnityEngine;
using System.Collections;

/*
 *Call either big ship() or small ship()
 * when instantiating so that AI moves properly
 */


public class BigEnemyController : MonoBehaviour {

    //Big Ship moving up and down
    //public Vector3 lastPosition;
    //public Vector3 upLastPos;
    //public Vector3 downLastPos;
    ////public Vector3 vectorReset = Vector3.zero;
    //public float DistanceTraveled;
    //public float upDistanceTraveled;
    //public float downDistanceTraveled;
    //public float upDownDistance;
    //public float maxDistance = 20f;
    //public float maxUpDistance;
    //public float maxDownDistance;
    public float speed = 10f;

    //public float upMov;

    //public bool run;
    //public float runTime;
    //public Transform target;
    public GameObject bigEnemyobj;
    public Transform pointA;
    public Transform pointB;
    public Transform currentPoint;
    //public bool pointAHit;
    //public bool pointBhit;
    public GameObject PointsT;
    public Transform firePoint;
    //static public bool isFired;
    //small ship
   // static public bool smallShip;

    //beginning variable to tell whether it just started or not
    public bool begin;
    public bool beginTwo;

    public float flySpeed = 200f;

    public GameObject warpEffect;

    public GameObject enemProjectile;
    [SerializeField]
    private GameObject FiredProjectile;
	// Use this for initialization
	void Start () {
        //lastPosition = transform.position;
        //upLastPos = transform.position;
        //bigEnemyobj.GetComponent<Rigidbody>().AddForce(bigEnemyobj.GetComponent<Rigidbody>().transform.right * speed * 20);
        //transform.position = Vector3.MoveTowards(transform.position, pointA.position, flySpeed);
        currentPoint = pointA;
        //isFired = false;
        WaveManager.addEnemySpawn(1);

	}
	
	// Update is called once per frame

	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, currentPoint.position, flySpeed * Time.deltaTime);
        PointsT.GetComponent<Rigidbody>().AddForce(-bigEnemyobj.GetComponent<Rigidbody>().transform.right * speed * 10);
        GetComponent<Rigidbody>().AddForce(-GetComponent<Rigidbody>().transform.right * speed * 10);
        if (transform.position.y == pointA.position.y)
        {
            currentPoint = pointB;
        }
        else if (transform.position.y == pointB.position.y)
        {
            currentPoint = pointA;
        }
        if(FiredProjectile == null)
        {
            FireProjetile();
        }
        //if (isFired == false)
        //{
        //    Instantiate(enemProjectile, firePoint.position, Quaternion.identity);
        //    isFired = true;
            
        //}
        
                
            //bigEnemyobj.GetComponent<Rigidbody>().AddForce(-bigEnemyobj.GetComponent<Rigidbody>().transform.right * speed * 20);
        //if (begin == true)
        //{
        //    transform.position = Vector3.MoveTowards(transform.position, pointA.position, flySpeed);
        //    if (transform.position.y == pointA.position.y)
        //    {
        //        pointAHit = true;
        //        begin = false;
        //        beginTwo = true;
        //    }

        //}
        
        //if (beginTwo == true)
        //{
            
        //        transform.position = Vector3.MoveTowards(transform.position, pointA.position, flySpeed);
        //    //else if ()
        //    //begin = false;
        //}
        //GetComponent<Rigidbody>().AddForce(-transform.right * speed * 20);

        //StartCoroutine(BigShipMove(runTime));

        ////smallShip = false;
        //if(begin == true)
        //{
        //    upLastPos = transform.position;
        //    //move up
        //    GetComponent<Rigidbody>().AddForce(transform.up * speed);
        //    GetComponent<Rigidbody>().AddForce(-transform.right * speed);
        //    begin = false;
        //    Debug.Log("Begin == true if statement");
        //    upDistanceTraveled += Vector3.Distance(transform.position, upLastPos);
        //}


        //DistanceTraveled += Vector3.Distance(transform.position, lastPosition);


        //if(upDistanceTraveled >= maxUpDistance)
        //{
        //    downLastPos = transform.position;
        //    upDistanceTraveled = 0;
        //    //move down
        //    GetComponent<Rigidbody>().AddForce(-transform.up * speed);
        //    Debug.Log("Going down");
        //    downDistanceTraveled += Vector3.Distance(transform.position, downLastPos);
        //}
        //else if(downDistanceTraveled >= maxDownDistance)
        //{
        //    upLastPos = transform.position;
        //    downDistanceTraveled = 0;
        //    //move up
        //    GetComponent<Rigidbody>().AddForce(transform.up * speed);
        //    Debug.Log("going up");
        //    upDistanceTraveled += Vector3.Distance(transform.position, upLastPos);
        //}
        //else if (DistanceTraveled >= maxDistance)
        //{
        //    Destroy(gameObject);
        //    WaveManager.addAstroidDestroy(1);
        //    Debug.Log("Going Destroy");
        //}
        //if(pointA.GetComponent<Collider>)

    }

    void FireProjetile()
    {
        var newProjectile = Instantiate(enemProjectile, firePoint.position, Quaternion.identity);
        FiredProjectile = newProjectile;
    }
    

    //IEnumerator BigShipMove(float _waitTime)
    //{
    //    //Transform currentPos;
    //    //currentPos = transform.position;
    //    do
    //    {
    //        //GetComponent<Rigidbody>().AddForce(transform.up * speed, ForceMode.Impulse);
    //        //Debug.Log("Going up");
    //        //GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, 10, 0);

    //        transform.position = Vector3.MoveTowards(transform.position, pointA.position, 1);
    //        //transform.position = new Vector3(GetComponent<Rigidbody>().transform.position.x, 10, 0);


    //        yield return new WaitForSeconds(_waitTime);
    //        //GetComponent<Rigidbody>().velocity = Vector3.zero;
    //        //yield return new WaitForSeconds(.1f);


    //        //GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, 0, 0);
    //        //Debug.Log("Going down");
    //        //GetComponent<Rigidbody>().AddForce(0, 0, 0);
    //        //GetComponent<Rigidbody>().AddForce(-transform.up * speed, ForceMode.Impulse);

    //        //GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x, -10, 0);
    //        //transform.position = new Vector3(GetComponent<Rigidbody>().transform.position.x, -10, 0);
    //        transform.position = Vector3.MoveTowards(transform.position, pointB.position, 1);
    //        yield return new WaitForSeconds(_waitTime);
    //        //GetComponent<Rigidbody>().velocity = Vector3.zero;
    //        //yield return new WaitForSeconds(.1f);
    //        //GetComponent<Rigidbody>().AddForce(0, 0, 0);

    //    } while (run == true);

    //}
}
