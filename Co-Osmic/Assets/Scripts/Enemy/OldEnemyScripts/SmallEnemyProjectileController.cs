﻿using UnityEngine;
using System.Collections;

public class SmallEnemyProjectileController : MonoBehaviour {
    public float speed;
    public float rotationSpeed = 300f;
    public float maxDistance = 20f;
    public float DistanceTraveled;

    public GameObject expdEffect;

    //[SerializeField]
    //private GameObject player;

    public Transform target;
    public Transform myTransform;
   // private Transform lastPPos;
    public Vector3 lastPosition;

    public int damageToGive;

    public bool locked;
	// Use this for initialization
	void Start () {

        //player = GameObject.Find("mPlayer");
        Physics.IgnoreLayerCollision(12, 13, true);
        lastPosition = transform.position;
        //var rotation = new Vector3(0, 0, Random.Range(-360, 360));
        //transform.Rotate(rotation);
        //SmallEnemyController.isFired = true;
        //player.GetComponent<Rigidbody>().position = lastPPos.position;
        //playerPos.position = player.transform.position;
        //player.transform.position = playerPos.position;
        //lastPPos = playerPos;
        locked = false;
        //Instantiate(playerPos, player.transform.position, Quaternion.identity);
        target = GameObject.FindWithTag("mPlayer").transform; //target the player
	}
    void Awake()
    {
        myTransform = transform; //cache transform data for easy access preformance
        
    }
	
	// Update is called once per frame
	void Update () {
        if (locked)
        {
            //playerPos.position = player.transform.position;
            //transform.position = Vector3.MoveTowards(lastPosition, player.transform.position, speed * Time.deltaTime);

            //rotate towards player
            

            //move towards the player
            myTransform.position += myTransform.forward * speed * Time.deltaTime;

            DistanceTraveled += Vector3.Distance(transform.position, lastPosition);
            //Debug.Log("Locked and supposedly working");
            if (DistanceTraveled >= maxDistance)
            {
                Destroy(gameObject);
                //SmallEnemyController.isFired = false;
            }
        }
        else if(!locked)
        {
            //myTransform.rotation = Quaternion.Lerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
            myTransform.rotation = Quaternion.Lerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed);
            //Debug.Log("Locked equals false");
            //playerPos.position = player.transform.position;
            //player.transform.position = playerPos.position;
            locked = true;

        }
	}

    void OnTriggerEnter(Collider _collider)
    {
        if(_collider.tag == "Shield")
        {
            Destroy(gameObject);
            //SmallEnemyController.isFired = false;
        }
        if(_collider.tag == "mPlayer")
        {
            //hurt player function
            Destroy(gameObject);
            //SmallEnemyController.isFired = false;
        }
        //if (_collider.tag == "LargeAstroid" || _collider.tag == "MediumAstroid" || _collider.tag == "SmallAstroid")
        //{
        //    SmallEnemyController.isFired = false;
        //}
    }
    //void OnDestroy()
    //{
    //    SmallEnemyController.isFired = false;
    //}
}
