﻿using UnityEngine;
using System.Collections;

public class SmallEnemyController : MonoBehaviour {

    public float speed = 10;

    public Transform firePoint;

    //static public bool isFired;

    public GameObject enemProjectile;
    public GameObject warpEffect;

    [SerializeField]
    private GameObject FiredProjectile;

    

    void Start()
    {
        //isFired = false;
        WaveManager.addEnemySpawn(1);
    }

    void Update()
    {
         GetComponent<Rigidbody>().AddForce(-GetComponent<Rigidbody>().transform.right * speed  * Time.deltaTime, ForceMode.VelocityChange);
        if (FiredProjectile == null)
        {
            FireProjectile();
        }
         //if (isFired == false)
         //{
         //    Instantiate(enemProjectile, firePoint.position, Quaternion.identity);
         //    isFired = true;
         //}
    }

    void FireProjectile()
    {
        var newProjectile = Instantiate(enemProjectile, firePoint.position, Quaternion.identity);
        FiredProjectile = newProjectile;
    }

}
