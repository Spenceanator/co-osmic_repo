﻿ using UnityEngine;


public class EnemyHit : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public int scoreBigDestroy = 65;
    public int scoreSmallDestroy = 115;
    public int scoreBigHalfDestroy = 32; //For when you ram the enemy with the shield
    public int scoreSmallHalfDestroy = 60; //for when you ram the enemy with the shield

    public GameObject ExplodeParticle;

    public Transform InstantiatePoint;

    void OnTriggerEnter(Collider _collider)
    {
        if(_collider.tag == "LargeAstroid" || _collider.tag == "MediumAstroid" || _collider.tag == "SmallAstroid")
        {
            //add no points
            Destroyed();
        }
        if(_collider.tag == "PlayerProjectile")
        {
            Destroyed();
            Destroy(_collider.gameObject);
            if(gameObject.name == "BigEnemyObject")//add amount to player score
            {
                ScoreManager.AddScore(scoreBigDestroy);
                Debug.Log("Destroyed big enemy added 65 points");
            }
            else if(gameObject.name == "SmallEnemy")
            {
                ScoreManager.AddScore(scoreSmallDestroy);
                Debug.Log("Destroyed small enemy added 115 points");
            }
            
        }
        if(_collider.tag == "Shield")
        {
            Destroyed();
            if (gameObject.name == "BigEnemyObject")//add half of points to player score
            {
                ScoreManager.AddScore(scoreBigHalfDestroy);
                Debug.Log("Destroyed big enemy added 65 points");
            }
            else if (gameObject.name == "SmallEnemy")
            {
                ScoreManager.AddScore(scoreSmallHalfDestroy);
                Debug.Log("Destroyed small enemy added 115 points");
            }
            
        }
        

        
    }
    
    void Destroyed()
    {
        Instantiate(ExplodeParticle, InstantiatePoint.position, InstantiatePoint.rotation);
        if(transform.parent != null)
        {
            Destroy(transform.parent.gameObject);
        }
        //WaveManager.addEnemyDestroy(1);
        Destroy(gameObject);
        //WaveManager.CheckWaveOver();
    }
    void OnDestroy()
    {
        WaveManager.addEnemyDestroy(1);
        WaveManager.CheckWaveOver();
    }

}
