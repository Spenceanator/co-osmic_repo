﻿using UnityEngine;
using System.Collections;

public class EnemyExplosionTimer : MonoBehaviour {

    [SerializeField]
    private float wait = 1f;

    IEnumerator waitDestroy(float _wait)
    {
        yield return new WaitForSeconds(_wait);
        Destroy(gameObject);
        
    }

    void Awake()
    {
        StartCoroutine(waitDestroy(wait));
    }
}
