﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipHit : MonoBehaviour {

    [SerializeField]
    private float wTime = 2f;

    public int damageToGive;

    public bool leftEngineHit; //when set to true say it is hit and damaged, then set hit to false
    public bool rightEngineHit;


    //public GameObject leftECol; //only used for setting collision to off
    //public GameObject rightECol;

    public bool canHit;
    public bool shipIsHit;

    public bool allDamaged;
    //public bool liDamaged;
    public bool tookDamage;

    private BlinkHit blinker;
    //private HitShield myHitShield;
    private EnemyShieldMotor myShieldMotor;
    private EnemyShipController myShipController;
    private EnemyHealthManager myHealthManager;

    // Use this for initialization
    void Awake()
    {
        canHit = true;
        shipIsHit = false;
        allDamaged = false;

        //liDamaged = false;

        blinker = gameObject.GetComponentInParent<BlinkHit>();
        myShipController = gameObject.GetComponentInParent<EnemyShipController>();
        myHealthManager = gameObject.GetComponentInParent<EnemyHealthManager>();
        myShieldMotor = gameObject.GetComponentInParent<EnemyShieldMotor>();
        

    }

    // Update is called once per frame
    void Update()
    {

        //if (shipIsHit == true)
        //{
        //    canHit = false;
        //    GetComponent<Collider>().enabled = false;
        //    //if(leftEngineHit == false)
        //    //{
        //    //    leftECol.GetComponent<Collider>().enabled = false;
        //    //}
        //    //if(rightEngineHit == false)
        //    //{
        //    //    rightECol.GetComponent<Collider>().enabled = false;
        //    //}

        //}
        //else if (shipIsHit == false)
        //{
        //    canHit = true;
        //    GetComponent<Collider>().enabled = true;
        //    //if (leftEngineHit == false)
        //    //{
        //    //    leftECol.GetComponent<Collider>().enabled = true;
        //    //}
        //    //if (rightEngineHit == false)
        //    //{
        //    //    rightECol.GetComponent<Collider>().enabled = true;
        //    //}   
        //}
    }

    public IEnumerator SetCollision(float _w)
    {
        shipIsHit = true;
        canHit = false;
        GetComponent<Collider>().enabled = false;
        yield return new WaitForSeconds(_w);
        shipIsHit = false;
        canHit = true;
        GetComponent<Collider>().enabled = true;
    }

    void OnTriggerEnter(Collider other)
    {

        if (canHit)
        {
            tookDamage = false;
            canHit = false;
            blinker.allBlinking = true;
            //Debug.Log("Ship Hit triggered");
            shipIsHit = true;
            //Debug.Log(roomNumbers);
            //myShieldMotor.shieldHit = true; //HitShield.isHit = true;
            if (myShieldMotor != null)
            {
                StartCoroutine(myShieldMotor.SetCollision(wTime)); //Tell shields to set their co-routine
            }
            StartCoroutine(SetCollision(wTime));
            //if (other.tag == "EnemyProjectile" || other.tag == "SmallEnemyProjectile")
            //{
            //    Destroy(other.gameObject);

            //}
        }
      
    }

    public void TriggerHit()
    {
        if (canHit)
        {
            tookDamage = false;
            canHit = false;
            blinker.allBlinking = true;
            //Debug.Log("Ship Hit triggered");
            shipIsHit = true;
            //Debug.Log(roomNumbers);
            //myShieldMotor.shieldHit = true; //HitShield.isHit = true;
            if (myShieldMotor != null)
            {
                StartCoroutine(myShieldMotor.SetCollision(wTime)); //Tell shields to set their co-routine
            }
            StartCoroutine(SetCollision(wTime));
            //if (other.tag == "EnemyProjectile" || other.tag == "SmallEnemyProjectile")
            //{
            //    Destroy(other.gameObject);

            //}
        }
    }

    public void PlayerHit(int _dmg)
    {
        if(myHealthManager.currentHealth - _dmg <= 0)
        {
            myShipController.GivePoints();
        }
    }

    void OnDisable()
    {
        canHit = true;
        shipIsHit = false;
        allDamaged = false;
    }
}
